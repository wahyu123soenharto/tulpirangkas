<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/fasilitas-umum.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Fasilitas</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Fasilitas</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<section class="service-style2-area service-page2">
    <div class="container service-box">
        <div class="sec-title text-center">
          
            <div class="big-title black-clr"><h1>Fasilitas Umum</h1></div>
        </div>
        <div class="row">
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/umum/unnamed.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Taman Bermain</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">Taman Bermain</a></h3>
                                    </div>
                                    <p>Area ruang terbuka yang dirancang agar anak-anak dan anggota keluarga dapat berinteraksi dan bermain di dalam area perumahan.</p>      
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/umum/taman_hijau.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Taman Hijau</a></h3>
                            </div>
                           
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">Taman Hijau</a></h3>
                                    </div>
                                    <p>Ruang hijau terbuka yang dirancang agar para penghuni perumahan dan anggota keluarga dapat berinteraksi dan berkumpul di dalam area perumahan.</p>      
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/umum/masjid.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Masjid</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                              
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">Masjid</a></h3>
                                    </div>
                                    <p>Salah satu fasilitas sarana ibadah yang disediakan bagi para penghuni perumahan.
</p>      
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->     
            
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/umum/lapangan_olahraga.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Lapangan Olahraga</a></h3>
                            </div>
                           
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">Lapangan Olahraga</a></h3>
                                    </div>
                                    <p>Ruang terbuka non hijau yang berfungsi sebagai tempat melakukan aktivitas olahraga bagi para penghuni perumahan.</p>      
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->     
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/umum/kawasan_niaga.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Kawasan Niaga</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">Kawasan Niaga</a></h3>
                                    </div>
                                    <p>Tersedia Deretan Ruko yang dapat digunakan sebagai properti untuk investasi dalam meningkatkan bisnis Anda.</p>      
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->     
                       
        </div>
       
    </div>    
</section>
<section class="service-style2-area service-page2">
    <div class="container service-box">
        <div class="sec-title text-center">
            
            <div class="big-title black-clr"><h1>Fasilitas Sosial</h1></div>
        </div>
        <div class="row">
            
        <!--Start Single Service Style2-->
        <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/terminal_mandala.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Terminal Mandala</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                              
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">2 menit ke Terminal Bus Mandala</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
        
               <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/stasiun.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Stasiun Kereta</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">7 menit ke Stasiun Rangkasbitung</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/alun-alun.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Alun Alun Rangkas</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">5 Menit ke Puspemkab Lebak (alun-alun)</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->

             <!--Start Single Service Style2-->
             <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/tol.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Pintu Tol Rangkas</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">3 menit dari Pintu Tol Rangkas</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->

            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/poltekes.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">kampus POLTEKKES BANTEN</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">3 menit ke kampus POLTEKKES BANTEN</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->

        <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/latansah.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Universitas Latansa</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">10 Menit ke Universitas Latansa</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            
             
            
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/pasar.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Pasar Barata</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">15 Menit ke Pasar Barata</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->     
            <!--Start Single Service Fasilitas-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/rs.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Rumah Sakit MISI</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">20 menit ke Rumah Sakit MISI
</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->     

            <!--Start Single Service Fasilitas-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/rsud.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">RSUD Aji Darmo</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                               
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">5 menit ke RSUD Aji Darmo</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->   

            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/Fasilitas/sosial/Puskesmas.jpg" alt="Awesome Image">
                        <div class="static-content">
                            <div class="title">
                                <h3><a href="#">Puskesmas Mandala</a></h3>
                            </div>
                            
                        </div>
                        <div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="services-single.html">5 Menit ke Puskesmas Mandala</a></h3>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->               
        </div>
       
    </div>    
</section>

<?php 
include 'footer.php';
?>