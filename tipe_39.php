<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/39/tipe-39.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Rumah Tipe 39</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Rumah Tipe 39</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Start breadcrumb area-->


<br><br>

<section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="team-singel-image-box">
                    <img src="element/images/galery/Depan_rumah_tipe_39.jpg" alt="Awesome Image">    
                </div>    
            </div> 
            <div class="col-xl-6">
            <div class="team-singel-image-box">
                    <img src="element/images/galery/Belakang_rumah_tipe_39.jpg" alt="Awesome Image">    
                </div>  
            </div>       
        </div>
       

       </section>

       <br><br>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/39/FULL_DENAH_TYPE_39.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
       
    
   


       
<?php 
include 'footer.php';
?>