<?php 
include 'head.php';
?>

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(element/images/kontak1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Hubungi Kami</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Hubungi Kami</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End breadcrumb area-->

<!--Start Contact Info Area-->
<section class="contact-info-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4">
                <div class="single-contact-info-box text-center">
                    <div class="icon"><span class="flaticon-headphones"></span></div>
                    <div class="title">
                        <h3>Our Phone</h3>
                        <ul>
                            <!--li><a href="tel:+62818 671 900">0818 671 900</a></li-->
                            <li><a href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20saya%20ingin%20menanyakan%20mengenai%20info%20rumah">+62818 671 900</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-contact-info-box text-center">
                    <div class="icon"><span class="flaticon-mail-1"></span></div>
                    <div class="title">
                        <h3>Our Mail Box</h3>
                        <ul>
                            <li><a href="mailto:info@templatepath.com">info@tuliprangkas.com</a></li>
                            <li><a href="mailto:info@templatepath.com">marketing.info@tuliprangkas.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-contact-info-box text-center">
                    <div class="icon"><span class="flaticon-pin-1"></span></div>
                    <div class="title">
                        <h3>Location</h3>
                        <p>JL. Maulana Hasanudin, Desa Kaduagung Timur, Lebak Banten</p>
                    </div>
                </div>
            </div>  
                 
        </div>
    </div>
</section>
<!--End Contact Info Area-->

<!--Start Penawaran Promo-->
<section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="slogan-content wow slideInUp" data-wow-delay="100ms">
                    <div class="title">
                        <h1 style="font-size: 38px;">Mau Dapatkan Promo Dengan Harga Menarik?  </h1>
                    </div>
                    <div class="quote-button">
                        <a href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">Ya, Kirimi Saya Promo!<span class="flaticon-next"></span></a>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</section>
<!--End Penawaran Promo-->

<section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
           
            <div class="big-title black-clr"><h1>Team Marketing</h1></div>
        </div>
        <div class="row">
           
           
             <!--Start Single Team Member-->
             <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s;">
                <div class="single-team-member wow fadeInUp animated animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/willy(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Willy </h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Supervisor</span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700044777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
             
            
           
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s;">
                <div class="single-team-member wow fadeInUp animated animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/hasan.jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Hasanudin K</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                           <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700050777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member--> <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s;">
                <div class="single-team-member wow fadeInUp animated animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/berly(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Berly</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700049777 &amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
             
        </div>
    </div>
</section>


<br>

<!--Start Contact Form Section-->
<section class="contact-form-area">
    <div class="auto-container">
        <div class="row clearfix"> 
           
            <div class="col-xl-8 col-lg-7 col-md-12">
                <div class="contact-form">
                    <div class="title">
                        <h3>Permintaan Penawaran</h3>
                    </div>
                    <div class="inner-box">
                        <form id="contact-form" name="contact_form" class="default-form2" action="assets/inc/sendmail.php" method="post">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="input-box"> 
                                        <p>Nama:</p>  
                                        <input type="text" name="form_name" value="" placeholder="" required="">
                                    </div>      
                                </div>
                                <div class="col-xl-6">
                                    <div class="input-box">
                                        <p>Alamat Email :</p>   
                                        <input type="email" name="form_email" value="" placeholder="" required="">
                                    </div>      
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="input-box">
                                        <p>Subjek:</p>   
                                        <input type="text" name="form_subject" value="" placeholder="">
                                    </div>       
                                </div>
                                <div class="col-xl-6">
                                    <div class="input-box">
                                        <p>Telpon:</p>   
                                        <input type="text" name="form_phone" value="" placeholder="">
                                    </div>      
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="input-box"> 
                                        <p>Keterangan Penawaran Yang di Mau:</p>      
                                        <textarea name="form_message" placeholder="" required=""></textarea>
                                    </div>      
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="button-box">
                                        <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                        <button class="btn-one" type="submit" data-loading-text="Please wait...">Kirim Permintaan</button>    
                                    </div>  
                                </div>
                            </div>                        
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 col-lg-5 col-md-12">
                <div class="contact-information-box">
                    <div class="title">
                        <h3>Kontak Info</h3>
                        <p>Hubungi Team Marketing kami, Kami Siap Untuk Melayani Anda.</p>
                    </div>
                    <ul class="contact-us">
                        <li>
                            <div class="icon">
                                <span class="flaticon-pin-1"></span>
                            </div>
                            <div class="text">
                                <p>JL. Maulana Hasanudin, Desa Kaduagung Timur,<br> Lebak Banten</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <span class="flaticon-open-envelope-with-letter"></span>
                            </div>
                            <div class="text">
                                <a href="mailto:info@templatepath.com">infon@tuliprangkas.com</a>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <span class="flaticon-smartphone"></span>
                            </div>
                            <div class="text">
                                <a href="tel:+62818671900">0818 671 900</a>
                            </div>
                        </li>   
                    </ul>
                </div>    
            </div>

        </div>
    </div>
</section>
<!--End Contact Form Section-->



<section class="google-map-area">
    <div class="map-outer">
        <!--Map Canvas-->
        <div class="map-canvas"
            ><div style="overflow:hidden;width: 2100px;position: relative;"><iframe width="2100" height="500" src="https://maps.google.com/maps?hl=en&amp;q=jl.maulana hasanudin, desa kaduagung timur, lebak banten+(Tulip Rangkas Residence)&amp;ie=UTF8&amp;t=&amp;z=13&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Map by <a href="https://www.googlemapsembed.net/">Embed Google Maps</a> <a href="https://mp3juice.vip/">Mp3 Juice</a> </small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><!-- Embed code --><script type="text/javascript">(new Image).src="//googlemapsembed.net/get?r"+escape(document.referrer);</script><script type="text/javascript" src="https://googlemapsembed.net/embed"></script><!-- END CODE --><br />
        </div>
    </div>
</section>

<?php 
include 'footer.php';
?>