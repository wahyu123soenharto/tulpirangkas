<?php 
include 'head.php';
?>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v6.0'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/id_ID/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="101833254816633"
  theme_color="#6699cc"
    logged_in_greeting="Hallo! Ada yang bisa kami bantu?"
    logged_out_greeting="Hallo! Ada yang bisa kami bantu?">
</div>
<!-- Start Main Slider -->
<section class="main-slider style1">
    <div class="slider-box">
        <!-- Banner Carousel -->
        <div class="banner-carousel owl-theme owl-carousel">
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url(element/images/slides/slide-v1-1.jpg)"></div>
                <div class="auto-container">
                    <div class="content">
                        <h2>Promo <span>Tumah Tipe 54/120</span><br> </h2>
                        <h3><img src="assets/images/icon/slide-title-icon-1.png" alt="">Perumahan Tulip Rangkas Residence</h3>
                        <div class="text">
                            <p>
                                3 Kamar Tidur / 2 Kamar Mandi / 1 Parkir Mobil / 1 Dapur 
                            </p>
                        </div>
                        <div class="btns-box">
                            <!--a href="#" class="btn-two">Pesan Sekarang</a-->
                            <a class="btn-one" href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">Pesan Sekarang<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url(element/images/slides/slide-v1-2.jpg)"></div>
                <div class="auto-container">
                    <div class="content">
                        <h2>Tipe Rumah <span>39/60</span><br> </h2>
                        <h3><img src="assets/images/icon/slide-title-icon-1.png" alt="">Perumahan Tulip Rangkas Residence</h3>
                        <div class="text">
                            <p>
                                2 Kamar Tidur / 1 Kamar Mandi / 1 Parkir Mobil / 1 Dapur 
                            </p> 
                        </div>
                        <div class="btns-box">
                               <!--a href="#" class="btn-two">Pesan Sekarang</a-->
                               <a class="btn-one" href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">Pesan Sekarang<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slide -->
            <div class="slide">
                <div class="image-layer" style="background-image:url(element/images/slides/slide-v1-3.jpg)"></div>
                <div class="auto-container">
                    <div class="content">
                        <h2>Tipe Rumah <span>27/60</span><br> </h2>
                        <h3><img src="assets/images/icon/slide-title-icon-1.png" alt="">Perumahan Tulip Rangkas Residence</h3>
                        <div class="text">
                            <p>
                                2 Kamar Tidur / 1 Kamar Mandi / 1 Parkir Mobil 
                            </p>
                         </div>
                        <div class="btns-box">
                            <!--a href="#" class="btn-two">Get a Quote</a-->
                            <a class="btn-one" href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">Pesan Sekarang<span class="flaticon-next"></span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Main Slider -->
<!--Start About Style1 Area-->
<section class="about-style1-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
                <div class="about-style1-image-box clearfix">
                    <div class="shape zoom-fade"></div>
                    <div class="image-box1">
                        <img src="element/images/perumahan.png" alt="Awesome Image">
                    </div>
                    
                </div>     
            </div>
            <div class="col-xl-7 col-lg-12">
                <div class="about-style1-text-box">
                    <div class="title">
                        <!--h2>WELCOME</h2-->
                        <h1>Selamat Datang  <span>Di Perumahan</span><br> <b>Tulip</b>Rangkas<br> Residence.</h1>
                    </div>
                    <div class="inner-contant">
                        <p>Tulip Rangkas Residence adalah sebuah Perumahan Komersil dan Subsidi yang dibangun oleh PT. Java Swiss International pada tahun 2019 yang terletak di Rangkasbitung, Lebak, Banten. Perumahan ini mempunyai 2 Type Perumahan Komersil yaitu Type 54/120 dan Type 39/60, serta untuk Type Perumahan Subsidi dengan tipe 27/60. 
Seperti Motto Kami A Comfort Place to Live, Tulip Rangkas Residence memberikan kenyamanan dan keamanan hunian untuk tempat tinggal dengan harga terjangkau dan dekat dengan fasilitas umum. Lokasi yang strategis dan dekat dengan beberapa akses publik seperti Sekolah, Stasiun, Pintu Tol, Universitas, Terminal, Pasar, Puskesmas, dan SPBU, akan menjadi nilai lebih untuk Anda dalam memilih rumah.
</p>
                        
                        <!--div class="signature-box">
                            <img src="element/images/signature.png" alt="Signature">    
                        </div-->
                    </div>    
                </div>  
            </div>
            
        </div> 
    </div>    
</section>
<!--End About Style1 Area-->
<!--Start Service Style2 Area-->

<section class="service-style2-area">
    <div class="layer-outer" style="background-image:url(element/images/joao-silas-8cMP8vj0bIA-unsplash.jpg);"></div>
    <div class="container service-box">
    <br><br>
        <div class="sec-title text-center">
            <p style="color: aliceblue; font-size: initial; " ;>A COMFORT PLACE TO LIVE</p>
            <div class="big-title"><h1>TULIP RANGKAS RESIDENCE</h1></div>
        </div>
        <div class="row">
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/service-2.jpg" alt="Awesome Image">
                        <!--div class="static-content">
                            <div class="title">
                                <h3><a href="#">Tipe Rumah 54/120</a></h3>
                            </div>
                            <div class="icon">
                                <a href="#"><span class="flaticon-plus"></span></a>
                            </div>
                        </div-->
                        <!--div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="#">Tipe Rumah 54/120</a></h3>
                                    </div>
                                    <p>perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Subsidi Tipe 54/120 dengan harga yang ditawarkan mulai dari IDR 399 jt. Lokasi strategis di kawasan Lebak, Banten. Kunjungi website kami di untuk info lebih detail.</p>      
                                </div>
                            </div>
                        </div-->
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/service-1.jpg" alt="Awesome Image">
                        <!--div class="static-content">
                            <div class="title">
                                <h3><a href="#">Tipe Rumah 39/60</a></h3>
                            </div>
                            <div class="icon">
                                <a href="#"><span class="flaticon-plus"></span></a>
                            </div>
                        </div-->
                        <!--div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="#">Tipe Rumah 39/60</a></h3>
                                    </div>
                                    <p>perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Subsidi Tipe 39/60 dengan harga yang ditawarkan mulai dari IDR 225 jt. Lokasi strategis di kawasan Lebak, Banten. Kunjungi website kami di untuk info lebih detail.</p>      
                                </div>
                            </div>
                        </div-->
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->
            <!--Start Single Service Style2-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style2">
                    <div class="img-holder">
                        <img src="element/images/service-3.jpg" alt="Awesome Image">
                        <!--div class="static-content">
                            <div class="title">
                                <h3><a href="#">Tipe Rumah 27/60</a></h3>
                            </div>
                            <div class="icon">
                                <a href="#"><span class="flaticon-plus"></span></a>
                            </div-->
                        </div>
                        <!--div class="overlay-content">
                            <div class="inner-content">
                                
                                <div class="text-holder">
                                    <div class="title">
                                        <h3><a href="#">Tipe Rumah 27/60</a></h3>
                                    </div>
                                    <p>perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Subsidi Tipe 27/60 dengan harga yang ditawarkan mulai dari IDR 150,5 jt. Lokasi strategis di kawasan Lebak, Banten. Kunjungi website kami di untuk info lebih detail.</p>      
                                </div>
                            </div>
                        </div-->
                    </div> 
                </div>
            </div>
            <!--End Single Service Style2-->          
        </div>
        <!--div class="row">
            <div class="col-xl-12">
                <div class="all-service-button text-center">
                    <a class="btn-one" href="#">Our All Services<span class="flaticon-next"></span></a>
                </div>
            </div>
        </div-->
    </div>    
</section>
<!--End Service Style2 Area-->
<!--Start Service Style1 Area-->
<section class="service-style1-area">
    <div class="container">
        <div class="title">
            <h1>Memberikan<br> Kesan Aman Dan Nyaman<br> </h1>
        </div>
        <div class="row">
            <!--Start Single Service Style1-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style1">
                    <div class="icon-holder">
                        <img src="element/images/service-icon-1.png" alt="Icon">
                    </div> 
                    <div class="text-holder">
                        <h3><a href="#">Suasana<br>Area</a></h3>
                        <p>Tulip Rangkas Residence juga fokus pada Green Area untuk memberikan suasana yang nyaman</p>
                        <div class="count-box">1</div>
                    </div>   
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style1">
                    <div class="icon-holder">
                        <img src="element/images/service-icon-2.png" alt="Icon">
                    </div> 
                    <div class="text-holder">
                        <h3><a href="#">Akses Publik &<br> Terdekat</a></h3>
                        <p>Tulip Rangkas Residence memiliki lokasi yang sangat strategis untuk menjalankan aktifitas</p>
                        <div class="count-box">2</div>
                    </div>   
                </div>
            </div>
            <!--End Single Service Style1-->
            <!--Start Single Service Style1-->
            <div class="col-xl-4 col-lg-4">
                <div class="single-service-style1">
                    <div class="icon-holder">
                        <img src="element/images/service-icon-3.png" alt="Icon">
                    </div> 
                    <div class="text-holder">
                        <h3><a href="#">Marketing <br> Yang Ramah</a></h3>
                        <p>Ingin mendapatkan harga menarik dari Marketing kami? Hubungi Sekarang</p>
                        <div class="count-box">3</div>
                    </div>   
                </div>
            </div>
            <!--End Single Service Style1-->
        </div>
    </div>
</section>
<!--End Service Style1 Area-->


<section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
           
            <div class="big-title black-clr"><h1>Team Marketing Kami</h1></div>
        </div>
        <div class="row">
           
            
             <!--Start Single Team Member-->
             <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1300ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/willy(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Willy </h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Supervisor</span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700044777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">
                                <span class="flaticon-whatsapp thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
           
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1500ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/hasan.jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Hasanudin K</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                           <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700050777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member--> <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1500ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/berly(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Berly</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700049777 &amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
           
          
        </div>
    </div>
</section>
<!--End Team Area-->  

<!--Start Penawaran Promo-->
<section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="slogan-content wow slideInUp" data-wow-delay="100ms">
                    <div class="title">
                        <h1 style="font-size: 38px;">Mau Dapatkan Promo Dengan Harga Menarik?  </h1>
                    </div>
                    <div class="quote-button">
                        <a href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank">Ya, Kirimi Saya Promo!<span class="flaticon-next"></span></a>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</section>
<!--End Penawaran Promo-->
<br><br>
<!--Start Fact Counter Area-->
<section class="fact-counter-area">
    <div class="container">
        <div class="row">
            <!--Start Single Fact Counter-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-fact-counter text-left wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="title">
                        <h3>Rumah Terjual<br> Tipe 54/120</h3>
                    </div>
                    <div class="count-box">
                        <h1>
                            <span class="timer" data-from="1" data-to="12" data-speed="5000" data-refresh-interval="50">30</span>
                        </h1>
                        <div class="icon"><span class="flaticon-plus"></span></div>
                    </div>
                </div>
            </div>
            <!--End Single Fact Counter-->
            <!--Start Single Fact Counter-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-fact-counter text-left wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="title">
                        <h3>Rumah Terjual<br> Tipe 39/60</h3>
                    </div>
                    <div class="count-box">
                        <h1>
                            <span class="timer" data-from="1" data-to="6" data-speed="5000" data-refresh-interval="50">25</span>
                        </h1>
                        <div class="icon"><span class="flaticon-plus"></span></div>
                    </div>
                </div>
            </div>
            <!--End Single Fact Counter-->
            <!--Start Single Fact Counter-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-fact-counter text-left wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="title">
                        <h3>Rumah Terjual<br> Tipe 27/60</h3>
                    </div>
                    <div class="count-box">
                        <h1>
                            <span class="timer" data-from="1" data-to="48" data-speed="5000" data-refresh-interval="50">99</span>
                        </h1>
                        <div class="icon"><span class="flaticon-plus"></span></div>
                    </div>
                </div>
            </div>
            <!--End Single Fact Counter-->
            <!--Start Single Fact Counter-->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="single-fact-counter text-left wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="title">
                        <h3>Ruko<br> Terjual</h3>
                    </div>
                    <div class="count-box">
                        <h1>
                            <span class="timer" data-from="1" data-to="2" data-speed="5000" data-refresh-interval="50">74</span>
                        </h1>
                        <div class="icon"><span class="flaticon-plus"></span></div>
                    </div>
                </div>
            </div>
            <!--End Single Fact Counter-->
        </div>
    </div>
</section>   
<!--End Fact Counter Area--> 

<!--Start Testimonial -->
<section class="testimonial-style2-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>Apa Kata Pelanggan</h1></div>
        </div>
        <div class="row"> 
            <div class="col-xl-12">
                <div class="rinbuild-carousel testimonial-carousel owl-carousel owl-theme owl-dot-style1" data-options='{"loop":true, "margin":30, "autoheight":true, "nav":false, "dots":true, "autoplay":true, "autoplayTimeout":6000, "smartSpeed":500, "responsive":{ "0":{"items": "1"}, "768":{"items": "1"}, "1000":{"items": "2" }}}'>
                    <!--Start Single Testimonial Style2-->
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Saya ambil rumah di Tulip Rangkas Residence karena lokasi strategis dekat sekolah anak saya, dekat dengan jalan tol yang hanya beberapa menit saja. Dan pastinya infrastruktur keren serta dengan jalannya lebar</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>ibu ati</h3>
                                <p>PNS Setda Lebak.</p>
                            </div>
                        </div>   
                    </div>
                    <!--End Single Testimonial Style2--> 
                    <!--Start Single Testimonial Style2-->
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Perumahan Untuk Jangka panjang yg bagus, terimakasi Tulip Rangkas Residence.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Kiki Rizkiani</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <!--End Single Testimonial Style2-->
                    <!--Start Single Testimonial Style2-->
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Wah Dp Gak terlalu mahal, Terimakasi.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Nina Herawati</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <!--End Single Testimonial Style2--> 
                    <!--Start Single Testimonial Style2-->
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Terimakasi Tulip Rangkas Akhirnya punya rumah</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Yanto Darmawan</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <!--End Single Testimonial Style2-->
                    <!--Start Single Testimonial Style2-->
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Mantap Dah Tulip, Akhirnya aku punya rumah sendiri.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Wahyu Soenharto</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <!--End Single Testimonial Style2-->              
                </div>
            </div> 
              
        </div>
    </div>
</section>
<!--End Testimonial --> 

 
   
<!--Start Team Area-->
 
<!--Start Partner Area-->
<section class="partner-area">
    <div class="container">
        <div class="sec-title text-center">
            
            <div class="big-title black-clr"><h1>MITRA BANK</h1></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="partner-box">
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/btn.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/btn.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/bri syariah.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/bri syariah.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box marleft-minus">
                        <a href="#"><img src="element/images/mitra/Logo_BNI_Syariah.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/Logo_BNI_Syariah.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/mandiri.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/mandiri.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                   
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End Partner Area-->  

<!--Start footer area-->  
<footer class="footer-area">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!--Start single footer widget-->
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.3s">
                    <div class="single-footer-widget">
                        <div class="our-company-info">
                            <div class="footer-logo">
                                <a href="index.html"><img src="element/images/logo/logo_putih.png" alt="Awesome Footer Logo" title="Logo"></a>    
                            </div>
                            <div class="text">
                                <p>Tulip Rangkas Residence hadir memberikan hunian yang sangat dekat dengan beberapa akses publik seperti Stasiun Rangkasbitung, Terminal, SPBU, hingga akses pendidikan.</p>
                            </div>    
                            <div class="footer-social-links">
                                <ul class="social-links-style1">
                                    <li>
                                        <a href="https://www.instagram.com/tuliprangkas.residence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                                    </li>
                                    
                                    <li>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End single footer widget-->
                <!--Start single footer widget-->
                <div class="col-xl-4 col-lg-4 col-md-9 col-sm-12 wow animated fadeInUp" data-wow-delay="0.5s">
                    <div class="single-footer-widget margin50-0">
                        <div class="title">
                            <h3>Menu</h3>
                        </div>
                        <div class="pages-box">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                <ul class="page-links">
                                        <li><a href="about-us.php">Tentang Kami</a></li>
                                        <li><a href="produk.php">Info</a></li>
                                        <li><a href="promo.php">Promo</a></li>
                                        <li><a href="tipe_rumah_54.ph">Galery</a></li>
                                        <li><a href="contact.php">Hubungi Kami</a></li>	
                                    </ul>     
                                </div>
                                <!-- <div class="col-xl-6 col-lg-6 col-md-6">
                                    <ul class="page-links">
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Departments</a></li>
                                        <li><a href="#">Timetable</a></li>
                                        <li><a href="#">Why Us</a></li>
                                        <li><a href="#">Specilaties</a></li>	
                                        <li><a href="#">Retail</a></li>	
                                    </ul>      
                                </div> -->
                            </div>    
                        </div>
                          
                    </div>
                </div>
                <!--End single footer widget-->  
                <!--Start single footer widget-->
                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.7s">
                    <div class="single-footer-widget">
                        <div class="twitter-feed-box">
                            <h3><a href="#">Update Unit Terjual</a></h3>
                            <span>14 Februari 2020</span>
                            <div class="border-box"></div> 
                            <img src="element/images/1.jpg" alt="Awesome Image" style="height: 200px;">
                            <div class="text">
                           </div>
                            <div class="bottom">
                                <div class="comments">
                                    <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>2 comments</a>
                                </div>
                                <!-- <div class="twitter-icon"> -->
                                    <!-- <span class="flaticon-twitter-logo-shape"></span> -->
                                <!-- </div> -->
                            </div>
                        </div>                   	  
                    </div>
                </div>
                <!--End single footer widget-->
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="outer-box">
                <div class="copyright-text">
                    <p>Copyright© <a href="#">PT Javasiss</a></p>
                </div>
                    
            </div>    
        </div>    
    </div>
</footer>   
<!--End footer area-->

</div> 


<button class="scroll-top scroll-to-target" data-target="html">
    <span class="icon-angle">
        <img src="element/images/icon/panah.png">
    </span>
</button>

<script src="element/js/jquery.js"></script>
<script src="element/js/aos.js"></script>
<script src="element/js/appear.js"></script>
<script src="element/js/bootstrap.bundle.min.js"></script>
<script src="element/js/bootstrap-select.min.js"></script>
<script src="element/js/isotope.js"></script>
<script src="element/js/jquery.bootstrap-touchspin.js"></script>
<script src="element/js/jquery.countdown.min.js"></script>
<script src="element/js/jquery.countTo.js"></script>
<script src="element/js/jquery.easing.min.js"></script>
<script src="element/js/jquery.enllax.min.js"></script>
<script src="element/js/jquery.fancybox.js"></script>
<script src="element/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="element/js/jquery.mixitup.min.js"></script>
<script src="element/js/jquery.paroller.min.js"></script>
<script src="element/js/jquery.polyglot.language.switcher.js"></script>
<script src="element/js/map-script.js"></script>
<script src="element/js/nouislider.js"></script>
<script src="element/js/owl.js"></script>
<!-- <script src="element/js/timePicker.js"></script>    -->
<script src="element/js/validation.js"></script>
<script src="element/js/wow.js"></script>
<script src="element/js/jquery.magnific-popup.min.js"></script>
<script src="element/js/slick.js"></script>
<script src="element/js/lazyload.js"></script>
<!-- thm custom script -->
<script src="element/js/custom.js"></script>
</body>
</html>