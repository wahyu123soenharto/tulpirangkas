<?php 
include 'head.php';
?>

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" >

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>SITE PLAN</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.html">PERUMAHAN TULIP RANGKAS RESIDENCE</li>
                           
                        </ul>    
                    </div>

                    <div class="icon">
                        <div class="inner">
                        <video autoplay style="width:1180px; height:700px; " >
<source src="element/images/RumahTipe54.mp4" type="video/mp4" >
</video>
                               
                            </a>
                        </div>   
                    </div>

                   
                </div>
            </div>
        </div>
    </div>
</section>
<!--End breadcrumb area-->

<!--Start latest blog area -->
<section class="blog-pagev2-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-7 col-md-12 col-sm-12">
                <div class="blog-post">
                    <!--Start Single Blog Post Style3-->
                    <div class="single-blog-post-style3 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1200ms">
                        <div class="img">
                            <img src="element/images/SITE PLANE/site plane.jpg" alt="Awesome Image">
                            
                        </div>
                        <div class="text-holder">
                            <ul class="meta-info">
                                <li><a href="#">20 Octobar,2019</a></li>
                                <li><span class="flaticon-chat"></span><a href="#">10 Comments</a></li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-single.html"> Denah Site Plane Perumahan Tulip Rangkas Residence.</a></h3>
                            <h4>In : Builder Construction</h4>
                            <p>You should be able to find several indispensable facts about motivation in the following paragraphs. If there’s at least one fact you didn’t know before, imagine the difference it might make. It’s so difficult to go on when everything seems to fail, isn’t it.</p>
                            
                        </div>
                    </div>
                    <br><br>
                    <div class="single-blog-post-style3 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1200ms">
                        <div class="img">
                            <img src="element/images/SITE PLANE/lokasi.jpg" alt="Awesome Image">
                           
                        </div>
                        <div class="text-holder">
                            <ul class="meta-info">
                                <li><a href="#">20 Octobar,2019</a></li>
                                <li><span class="flaticon-chat"></span><a href="#">10 Comments</a></li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-single.html"> Denah Lokasi Perumahan Tulip Rangkas Residence.</a></h3>
                            <h4>In : Builder Construction</h4>
                            <p>You should be able to find several indispensable facts about motivation in the following paragraphs. If there’s at least one fact you didn’t know before, imagine the difference it might make. It’s so difficult to go on when everything seems to fail, isn’t it.</p>
                            
                        </div>
                    </div>
                    <br><br>
                    <!--End Single Blog Post Style3-->
                   
                    
                     
                </div>   
            </div>
            <!--Start sidebar Wrapper-->
            
        </div>
        
        
    </div>
</section>
<!--End latest blog area-->


<?php 
include 'footer.php';
?>