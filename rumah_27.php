
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Portfolio Details 01|| RinBuild || Responsive HTML 5 Template</title>

	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
	<link rel="stylesheet" href="element/css1/aos.css">
	<link rel="stylesheet" href="element/css1/bootstrap.min.css">	
	<link rel="stylesheet" href="element/css1/imp.css">
	<link rel="stylesheet" href="element/css1/custom-animate.css">
	<link rel="stylesheet" href="element/css1/flaticon.css">
	<link rel="stylesheet" href="element/css1/font-awesome.min.css">
	<link rel="stylesheet" href="element/css1/owl.css">
	<link rel="stylesheet" href="element/css1/magnific-popup.css">
   
    <link rel="stylesheet" href="element/css1/color.css">
    <link rel="stylesheet" href="element/css1/style.css">
	<link rel="stylesheet" href="element/css1/responsive.css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="element/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="element/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="element/images/favicon/favicon-16x16.png" sizes="16x16">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    
</head>

<body>

<!-- main header -->
<header class="main-header"> 
    <div class="header-top">
        <div class="container">
            <div class="outer-box">
                <div class="header-top-left">
                    <div class="welcome-text">
                        <h6><span class="flaticon-chat thm_clr1"></span>Our company has <b class="thm_clr1">20 years' experience!</b></h6> 
                    </div>       
                </div>
                <div class="header-top-middle">
                    <div class="header-social-links">
                        <ul class="social-links-style1">
                            <li>
                                <a href="#"><span class="flaticon-facebook-1 fb"></span></a> 
                            </li>
                            <li>
                                <a href="#"><span class="flaticon-twitter-1"></span></a> 
                            </li>
                            <li>
                                <a href="#"><span class="flaticon-instagram-1"></span></a> 
                            </li>
                            <li>
                                <a href="#"><span class="flaticon-linkedin-1"></span></a>
                            </li>
                        </ul>
                    </div>        
                </div>
                <div class="header-top-right">
                    <div class="header-menu">
                        <ul>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Terms of service</a></li>
                            <li><a href="#">Refund policy</a></li>
                        </ul>
                    </div>            
                </div>         
            </div>    
        </div>
    </div>
    
    <!--Start Header upper -->
    <div class="header-upper">
        <div class="container clearfix">
            <div class="outer-box clearfix"> 
                <div class="header-upper-left float-left clearfix">
                    <div class="logo">
                        <a href="index.html"><img src="assets/images/resources/logo.png" alt="Awesome Logo" title=""></a>
                    </div>
                </div>
                <div class="header-upper-right float-right clearfix">
                    <div class="header-contact-info">
                        <ul>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-call thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Call us anytime</span>
                                    <h6><a href="tel:123456789">(+99) 123456789</a></h6>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-clock thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Opening time</span>
                                    <h6>Sat - Sun 08.00 to 14.00</h6>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-mail thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Email us now</span>
                                    <h6><a href="mailto:info@templatepath.com">rinbuild@gmail.com</a></h6>
                                </div>
                            </li>
                        </ul>    
                    </div> 
                       
                </div>
            </div>
        </div>
    </div>
    <!--End Header Upper-->
    
    <!--Start header lawer -->
    <div class="header-lawer">
        <div class="container clearfix">
            <div class="outer-box clearfix">
                <!--Top Left-->
                <div class="header-lawer-left float-left">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="dropdown">
                                        <a class="home-icon" href="#"><span class="hometext">Home</span><span class="flaticon-real-estate homeicon"></span></a>
                                        <ul>
                                            <li><a href="index.html">Home Page 01</a></li>
                                            <li><a href="index-2.html">Home Page 02</a></li>
                                            <li><a href="index-3.html">Home Page 03</a></li>
                                            <li class="dropdown"><a href="#">Header Styles</a>
                                                <ul>
                                                    <li><a href="index.html">Header Style One</a></li>
                                                    <li><a href="index-2.html">Header Style Two</a></li>
                                                    <li><a href="index-3.html">Header Style Three</a></li>
                                                </ul>    
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#">About Us</a>
                                        <ul>
                                            <li><a href="about.html">About Style1 Page</a></li>
                                            <li><a href="about-v2.html">About Style2 Page</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#">Services</a>
                                        <ul>
                                            <li><a href="services.html">Services Style1 Page</a></li>
                                            <li><a href="services-v2.html">Services Style2 page</a></li>
                                            <li><a href="services-single.html">Services Details</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown current"><a href="#">Pages</a>
                                    	<ul>
                                            <li class="dropdown current"><a href="#">Portfolio</a>
                                                <ul>
                                                    <li><a href="portfolio-v1.html">Portfolio Style1 Page</a></li>
                                                    <li><a href="portfolio-v2.html">Portfolio Style2 Page</a></li>
                                                    <li><a href="portfolio-single-v1.html">Portfolio Details Style1</a></li>
                                                    <li><a href="portfolio-single-v2.html">Portfolio Details Style2</a></li>
                                                </ul>    
                                            </li>
                                            <li><a href="team.html">Team Page</a></li>
                                            <li><a href="team-single.html">Team Single Page</a></li>
                                            <li><a href="testimonials.html">Testimonials</a></li>
                                            <li><a href="client.html">Client Page</a></li>
                                            <li><a href="faq.html">Faq Page</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="#">Blog</a>
                                    	<ul>
                                            <li><a href="blog.html">Blog Grid View</a></li>
                                            <li><a href="blog-v2.html">Blog Arcive View</a></li>
                                            <li><a href="blog-single.html">Blog Single</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </nav>                        
                        <!-- Main Menu End-->
                    </div>      
                </div>
                <!--Top Right-->
                <div class="header-lawer-right clearfix float-right">
                    <div class="outer-search-box-style1">
                        <div class="seach-toggle"><i class="fa fa-search"></i></div>
                        <ul class="search-box">
                            <li>
                                <form method="post" action="index.html">
                                    <div class="form-group">
                                        <input type="search" name="search" placeholder="Search Here" required="">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>    
                    <div class="quote-button">
                        <a href="#">Get a Quote<span class="flaticon-next"></span></a>
                    </div>    
                </div>
            </div>  
        </div>
    </div>
    <!--End header lawer -->
   
    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="container">
            <div class="clearfix">
                <!--Logo-->
                <div class="logo float-left">
                    <a href="index.html" class="img-responsive"><img src="assets/images/resources/logo.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="right-col float-right">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="dropdown">
                                    <a class="home-icon" href="#"><span class="hometext">Home</span><span class="flaticon-real-estate homeicon"></span></a>
                                    <ul>
                                        <li><a href="index.html">Home Page 01</a></li>
                                        <li><a href="index-2.html">Home Page 02</a></li>
                                        <li><a href="index-3.html">Home Page 03</a></li>
                                        <li class="dropdown"><a href="#">Header Styles</a>
                                            <ul>
                                                <li><a href="index.html">Header Style One</a></li>
                                                <li><a href="index-2.html">Header Style Two</a></li>
                                                <li><a href="index-3.html">Header Style Three</a></li>
                                            </ul>    
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">About Us</a>
                                    <ul>
                                        <li><a href="about.html">About Style1 Page</a></li>
                                        <li><a href="about-v2.html">About Style2 Page</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                        <li><a href="services.html">Services Style1 Page</a></li>
                                        <li><a href="services-v2.html">Services Style2 page</a></li>
                                        <li><a href="services-single.html">Services Details</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown current"><a href="#">Pages</a>
                                    <ul>
                                        <li class="dropdown current"><a href="#">Portfolio</a>
                                            <ul>
                                                <li><a href="portfolio-v1.html">Portfolio Style1 Page</a></li>
                                                <li><a href="portfolio-v2.html">Portfolio Style2 Page</a></li>
                                                <li><a href="portfolio-single-v1.html">Portfolio Details Style1</a></li>
                                                <li><a href="portfolio-single-v2.html">Portfolio Details Style2</a></li>
                                            </ul>    
                                        </li>
                                        <li><a href="team.html">Team Page</a></li>
                                        <li><a href="team-single.html">Team Single Page</a></li>
                                        <li><a href="testimonials.html">Testimonials</a></li>
                                        <li><a href="client.html">Client Page</a></li>
                                        <li><a href="faq.html">Faq Page</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Blog</a>
                                    <ul>
                                        <li><a href="blog.html">Blog Grid View</a></li>
                                        <li><a href="blog-v2.html">Blog Arcive View</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </div>
    <!--End Sticky Header-->
</header>

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(assets/images/breadcrumb/breadcrumb-1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Portfolio Details 01</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.html">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Rumah Tipe 27</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End breadcrumb area-->

<!--Start Portfolio Single Style1 area-->
<section class="portfolio-single-style1-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="single-portfolio-slider">
                    <div class="row">
                        <div class="col-xl-8">
                            <ul class="slider-content clearfix bxslider">
                                <li>
                                    <div class="single-portfolio-slide clearfix">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div class="big-image-box">
                                                    <img src="element/images/portfolio/portfolio-slide-big-1.jpg" alt="Awesome Image">
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </li>
                                <li>
                                    <div class="single-portfolio-slide clearfix">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div class="big-image-box">
                                                    <img src="element/images/interior/27/dapur.jpg" alt="Awesome Image">
                                                </div>
                                            </div>
                                        </div>   
                                    </div>
                                </li>
                                <li>
                                    <div class="single-portfolio-slide clearfix">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                                <div class="big-image-box">
                                                    <img src="element/images/interior/27/dapur.jpg" alt="Awesome Image">
                                                </div>
                                            </div>
                                        </div>   
                                    </div>
                                </li>
                            </ul>    
                        </div>
                        <div class="col-xl-4">
                            <div class="slider-pager">
                                <ul class="thumb-box">
                                    <li>
                                        <a class="active" data-slide-index="0" href="#">
                                            <div class="img-holder">
                                                <img src="element/images/interior/27/dapur.jpg" alt="Awesome Image">    
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-slide-index="1" href="#">
                                            <div class="img-holder">
                                                <img src="element/images/portfolio/portfolio-slide-thumb-2.jpg" alt="Awesome Image">    
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-slide-index="2" href="#">
                                            <div class="img-holder">
                                                <img src="element/images/portfolio/portfolio-slide-thumb-3.jpg" alt="Awesome Image">    
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>    
                        </div>
                    </div>
                                        
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Portfolio Single Style1 area-->

<!--Start Portfolio Single Content Area-->
<div class="portfolio-single-content-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="portfolio-single-content">
                    <h3>Construction is Hard Work; Site Safety is Easy! | Total .</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen vived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was pop</p>
                    <p>1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishin Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's  text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen. vived not only five centuries, but also the leap into electronic typesetting.</p>
                </div>
                <div class="portfolio-single-video-box" style="background-image:url(assets/images/portfolio/portfolio-single-video-gallery.jpg);">
                    <a class="video-popup wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" title="RinBuild Video Gallery" href="https://www.youtube.com/watch?v=p25gICT63ek">
                        <span class="flaticon-play-button"></span>
                    </a>
                </div>
                <div class="single-portfolio-bottom-text">
                    <p>Donec scelerisque dolor id nunc dictum, interdum gravida mauris rhoncus. Aliquam at ultrices nunc. In sem leo, fermentum at lorem in, porta finibus mauris. Aliquam consectetur, ex in gravida porttitor,</p>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="project-info-box">
                    <h3>Our Project history.</h3>
                    <ul>
                        <li><span>Project Title:</span> Business Agency</li>
                        <li><span>Client:</span> Mahi Al Rabbi</li>
                    </ul>
                    <ul>
                        <li><span>Category:</span> Agency Title</li>
                        <li><span>Status:</span> Completed</li>
                    </ul>
                    <ul>
                        <li><span>Date:</span> 05 August 2019</li>
                        <li><span>Project Value:</span> $590</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
</div>
<!--End Portfolio Single Content Area-->
                        
                                                
<!--Start Related Portfolio Area-->
<section class="latest-portfolio-area related-portfolio">
    <div class="container">
        <div class="sec-title text-center">
            <p>Our Global Work Industries!</p>
            <div class="big-title black-clr"><h1>Related Portfolio</h1></div>
        </div>
        <div class="row">
            <!--Start Single portfolio Style1-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/1.jpg " alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="element/images/27/1.jpg">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Building</span>
                            <h5><a href="#">Building A Sports City</a></h5>
                            <p><span class="flaticon-location-pin"></span>KA-62/1, Kuril, Progoti</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single portfolio Style1-->
            <!--Start Single portfolio Style1-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="assets/images/portfolio/portfolio-v1-2.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="assets/images/portfolio/portfolio-v1-2.jpg">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Building</span>
                            <h5><a href="#">Building A Sports City</a></h5>
                            <p><span class="flaticon-location-pin"></span>KA-62/1, Kuril, Progoti</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single portfolio Style1-->
            <!--Start Single portfolio Style1-->
            <div class="col-xl-4 col-lg-4 col-md-12">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="assets/images/portfolio/portfolio-v1-3.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="assets/images/portfolio/portfolio-v1-3.jpg">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Building</span>
                            <h5><a href="#">Building A Sports City</a></h5>
                            <p><span class="flaticon-location-pin"></span>KA-62/1, Kuril, Progoti</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single portfolio Style1--> 
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="all-portfolio-button text-center">
                    <a class="btn-one" href="#">Our All Portfolio<span class="flaticon-next"></span></a>
                </div>
            </div>
        </div>
        
    </div>
</section>   
<!--End Related Portfolio Area-->                                                                                                 

<!--Start Partner Area-->
<section class="partner-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="partner-box">
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-1.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-1.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-2.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-2.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box marleft-minus">
                        <a href="#"><img src="assets/images/brand/brand-3.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-3.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-4.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-4.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-5.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-5.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-6.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-6.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-7.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-7.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-8.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-8.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box marleft-minus">
                        <a href="#"><img src="assets/images/brand/brand-9.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-9.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="assets/images/brand/brand-10.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="assets/images/brand/overlay-brand-10.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End Partner Area-->  

<!--Start footer area-->  
<footer class="footer-area">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!--Start single footer widget-->
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.3s">
                    <div class="single-footer-widget">
                        <div class="our-company-info">
                            <div class="footer-logo">
                                <a href="index.html"><img src="assets/images/footer/footer-logo.png" alt="Awesome Footer Logo" title="Logo"></a>    
                            </div>
                            <div class="text">
                                <p>Proin tempus, enim lobortis placerat porta, libero mauris feugiat magna, ut lobortis justo tortor a ipsum. Proin luctus posuere eros porttitor euismod. Praesent pulvinar.</p>
                            </div>    
                            <div class="footer-social-links">
                                <ul class="social-links-style1">
                                    <li>
                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End single footer widget-->
                <!--Start single footer widget-->
                <div class="col-xl-4 col-lg-4 col-md-9 col-sm-12 wow animated fadeInUp" data-wow-delay="0.5s">
                    <div class="single-footer-widget margin50-0">
                        <div class="title">
                            <h3>Information</h3>
                        </div>
                        <div class="pages-box">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <ul class="page-links">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Departments</a></li>
                                        <li><a href="#">Timetable</a></li>	
                                        <li><a href="#">Why Us</a></li>	
                                        <li><a href="#">Specilaties</a></li>	
                                    </ul>      
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <ul class="page-links">
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Departments</a></li>
                                        <li><a href="#">Timetable</a></li>
                                        <li><a href="#">Why Us</a></li>
                                        <li><a href="#">Specilaties</a></li>	
                                        <li><a href="#">Retail</a></li>	
                                    </ul>      
                                </div>
                            </div>    
                        </div>
                          
                    </div>
                </div>
                <!--End single footer widget-->  
                <!--Start single footer widget-->
                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.7s">
                    <div class="single-footer-widget">
                        <div class="twitter-feed-box">
                            <h3><a href="#">Etiam sapien tortor, dictum</a></h3>
                            <span>July 21, 2018 10:00 AM</span>
                            <div class="border-box"></div> 
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elieiusmod tempor incididunt ut labore et dolore magn aliqua. Ut enim ad minim veniam.</p>
                            </div>
                            <div class="bottom">
                                <div class="comments">
                                    <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>2 comments</a>
                                </div>
                                <div class="twitter-icon">
                                    <span class="flaticon-twitter-logo-shape"></span>
                                </div>
                            </div>
                        </div>                   	  
                    </div>
                </div>
                <!--End single footer widget-->
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="outer-box">
                <div class="copyright-text">
                    <p>Copyright© All Rights Reserved <a href="#">RinBuild.</a></p>
                </div>
                <div class="footer-menu">
                    <ul>
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Terms of service</a></li>
                        <li><a href="#">Refund policy</a></li>
                    </ul>
                </div>      
            </div>    
        </div>    
    </div>
</footer>   
<!--End footer area-->

</div> 


<button class="scroll-top scroll-to-target" data-target="html">
    <span class="icon-angle"></span>
</button>



<script src="element/js1/jquery.js"></script>
<script src="element/js1/aos.js"></script>
<script src="element/js1/appear.js"></script>
<script src="element/js1/bootstrap.bundle.min.js"></script>
<script src="element/js1/bootstrap-select.min.js"></script>
<script src="element/js1/isotope.js"></script>
<script src="element/js1/jquery.bootstrap-touchspin.js"></script>
<script src="element/js1/jquery.countdown.min.js"></script>
<script src="element/js1/jquery.countTo.js"></script>
<script src="element/js1/jquery.easing.min.js"></script>
<script src="element/js1/jquery.enllax.min.js"></script>
<script src="element/js1/jquery.fancybox.js"></script>
<script src="element/js1/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="element/js1/jquery.mixitup.min.js"></script>
<script src="element/js1/jquery.paroller.min.js"></script>
<script src="element/js1/jquery.polyglot.language.switcher.js"></script>
<script src="element/js1/map-script.js"></script>
<script src="element/js1/nouislider.js"></script>
<script src="element/js1/owl.js"></script>
<script src="element/js1/validation.js"></script>
<script src="element/js1/wow.js"></script>
<script src="element/js1/jquery.magnific-popup.min.js"></script>
<script src="element/js1/slick.js"></script>
<script src="element/js1/lazyload.js"></script>

<script src="assets/js1/jquery.bxslider.min.js"></script>

<!-- thm custom script -->
<script src="element/js1/custom.js"></script>



</body>
</html>