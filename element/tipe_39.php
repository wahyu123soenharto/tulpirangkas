<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(assets/images/breadcrumb/breadcrumb-1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Rumah Tipe 54</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.html">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Team Details</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Start breadcrumb area-->     
<section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="team-singel-image-box">
                    <img src="element/images/54/Type 54 120.jpg" alt="Awesome Image">    
                </div>    
            </div> 
            <div class="col-xl-4">
                <div class="team-member-info-box">
                    <div class="title">
                        <h3>Alex sundrop lipson</h3>
                        <span>CEO &amp; Founder</span>
                    </div>
                    <ul>
                        <li><span>Age:</span> 25 Years</li>
                        <li><span>Blod Group:</span> A+</li>
                    </ul>
                    <ul>
                        <li><span>Work progress:</span> 100%</li>
                        <li><span>Email:</span> <a href="mailto:info@templatepath.com">alex.lipson@gmail.com</a></li>
                    </ul>
                    <ul>
                        <li><span>Phone:</span> +555 666 77 88 99</li>
                    </ul>
                    <ul class="social-links-style1">
                        <li>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="flaticon-plus"></i></a>
                        </li>
                    </ul>
                </div>    
            </div>       
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="team-member-content-box">
                    <h3>My History</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen vived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was pop</p>
                    <p>1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishin Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                </div>
            </div>
        </div>

       </section>
       <section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>INTERIOR RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/54/REVISI-DENAH.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.4s" data-wow-duration="1400ms" style="visibility: visible; animation-duration: 1400ms; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/54/REVISI-DENAH.png" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/54/REVISI-DENAH.png" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
          
        </div>
    </div>
</section>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/54/REVISI-DENAH.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
   


       
<?php 
include 'footer.php';
?>