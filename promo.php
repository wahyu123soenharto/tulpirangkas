<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/projek-berjalan/anthony-fomin-sTw2KYpoujk-unsplash.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Promo!!</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Promo</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="service-imagegalley-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="top-single-service">
                    <div class="left">
                        <div class="icon">
                            <img style="height: 215px;" src="element/images/SPEAKER_1.jpg" alt="Icon">    
                        </div>
                        
                    </div>
                    <div class="right">
                        <p>Siapa bilang Tulip Rangkas Residence cuma punya rumah? Buat kamu yang sedang merintis usaha dan lagi nyari Ruko, kami juga menyediakan Ruko dengan harga dan promo menarik.
Penasaran seperti apa Ruko dan harganya?
Cek info lengkapnya dan hubungi team Marketing kami .</p>
                    </div>
                </div>
                <br>
                <div class="single-service">
                    <img src="element/images/single-ser-big-img-1.jpg" alt="Awesome Image">
                </div>
                
                
            </div>    
        </div>
    </div>    
</section>
<!--End Fact Counter Area--> 
<br><br>
<section class="service-imagegalley-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="top-single-service">
                    <div class="left">
                        <div class="icon">
                            <img style="height: 215px;" src="element/images/SPEAKER_Rumah.jpg" alt="Icon">    
                        </div>
                        
                    </div>
                    <div class="right">
                        <p>Promo Khusus, Stok Terbatas!
DP mulai dari 5 jutaan dengan cicilan super ringan. Hanya Rp 90ribu-an/hari bisa dapat rumah. Bebas Administrasi dokumen (BPHTB, AJB, BN & Biaya Proses KPR). Tunggu apalagi, pilih rumahnya dan DP sekarang sebelum kehabisan!</p>
                    </div>
                </div>
                <br>
                <div class="single-service">
                    <img src="element/images/TIPE39.jpg" alt="Awesome Image">
                </div>
                
                
            </div>    
        </div>
    </div>    
</section>
<!--End Fact Counter Area--> 
<br><br><br>
<!--Start Penawaran Promo-->
<section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="slogan-content wow slideInUp" data-wow-delay="100ms">
                    <div class="title">
                        <h1 style="font-size: 43px;"> Penawaran Promo dengan Harga Menarik? </h1>
                    </div>
                    <div class="quote-button">
                        <a href="#">Kirimi Saya Promo <span class="flaticon-next"></span></a>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</section>
<!--End Penawaran Promo-->
<br>
<section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
        
            <div class="big-title black-clr"><h1>Team Marketing Kami</h1></div>
        </div>
        <div class="row">
           
            
             <!--Start Single Team Member-->
             <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1300ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/willy(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Willy </h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Supervisor</span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700044777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
           
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1500ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/hasan.jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Hasanudin K</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                           <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700050777&amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member--> <div class="col-xl-4 col-lg-4 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1500ms">
                <div class="single-team-member wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="element/images/berly(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Berly</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700049777 &amp;text=Hallo%20,%20Saya%20tertarik%20untuk%20membeli%20rumah%20/%20ruko%20di%20Tulip%20Rangkas%20Residence.%20Mohon%20Kirimkan%20brosur%20dengan%20harga%20terbaik." target="_blank"><span class="flaticon-call thm_why"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--End Single Team Member-->
           
          
        </div>
    </div>
</section>
<!--End Team Area-->  

<?php 
include 'footer.php';
?>