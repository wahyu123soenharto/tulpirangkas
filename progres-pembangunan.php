<?php 
include 'head.php';
?>

<section class="breadcrumb-area" style="background-image: url(element/images/pembangunan.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Progres Pembangunan </h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Progres Pembangunan</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="latest-portfolio-area">
    <div class="container">
    <div class="sec-title text-center">
           
            <div class="big-title black-clr"><h1>Progress Pembangunan</h1></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="project-menu-box wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <ul class="project-filter clearfix post-filter has-dynamic-filters-counter">
                        <li data-filter=".filter-item" class="active"><span class="filter-text"><i class="flaticon-menu"></i>All</span></li>
                        <li data-filter=".ruko"><span class="filter-text"><i class="flaticon-building"></i>Ruko </span></li>
                        <li data-filter=".54"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 54</span></li>
                        <li data-filter=".39"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 39</span></li>
                        <li data-filter=".27"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 27</span></li>
               
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row filter-layout masonary-layout">
             <!--Start Single Tipe Rumah Ruko-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item ruko ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/progres/progres_ruko.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="title-holder">
                            <span class="tag">Ruko</span>
                            <h5><a href="#">Progres Pembangunan Ruko</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div-->
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->
             <!--Start Single Tipe Rumah 54-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 54 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/progres/progres_54.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div-->
                    </div>
                </div>
            </div>
             <!--End Single tipe 54-->

              

             <!--Start Single Tipe Rumah 39-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 39 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/progres/progres_39.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div-->
                    </div>
                </div>
            </div>
             <!--End Single tipe 39-->
            

            
            <!--Start Single Tipe Rumah 27-->
            <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/progres/progres_27.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--div class="title-holder">
                            <span class="tag">Perumahan</span>
                            <h5><a href="#">Tipe 54 / 120</a></h5>
                            <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div-->
                </div>
            </div>
            <!--End Single 27-->               
        </div>
        
    </div>
</section>   
<!--End Latest Portfolio Area--> 

<?php 
include 'footer.php';
?>