<?php 
include 'head.php';
?>
<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(element/images/city.jpg);">
  
<div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Tentang Kami</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Tentang Kami</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End breadcrumb area-->
   
<!--Start About Style1 Area-->
<section class="about-style1-area style3">
    <div class="about-style3-image-box" data-aos="fade-right" data-aos-duration="0" data-aos-delay="0">
        
    </div>  
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
            <img src="element/images/pak_Esam_Azubaidi.jpg" alt="Awesome Image" style="width: 800px;">
            </div>
            <div class="col-xl-7 col-lg-12">
                <div class="about-style1-text-box style3">
                    <div class="title">
                        <p></p>
                        <h1>Esam  Omar Mohamed Azzubaidi</h1>
                        <p style="font-size: 23PX;line-height: 20px;font-weight: 400;text-transform: uppercase;letter-spacing: 6px; margin:23px 0;">Board of Director</p>
                    </div>
                    <div class="inner-contant">
                        <p>Mengusung slogan "a comfort place to live", kami percaya bahwa kenyamanan Anda adalah prioritas utama kami dalam membangun Perumahan Tulip Rangkas Residence sebagai tempat tinggal yang nyaman untuk dihuni.</p>
                        <p>Bernaung di bawah bendera PT Java Swiss International, kami bertekad untuk senantiasa memenuhi kebutuhan masyarakat yang terus meningkat akan perumahan. Lokasi kami yang berada di lokasi strategis dan dekat dengan fasilitas umum sehingga memudahkan mobilitas Anda dalam beraktivitas dan bekerja.
                        <br> Karena kami percaya kebahagiaan keluarga berawal dari hunian yang nyaman.</p>
                    </div>    
                </div>  
            </div>
            
        </div> 
    </div>    
</section>
<!--End About Style1 Area-->



<!--Start Video Gallery Style2 Area-->
<section class="video-gallery-style2-area">
    <div class="video-galler-outer-bg" style="background-image:url(element/images/GATE 1 EDIT .png);">
        <div class="title-holder text-center">
            <div class="icon"><img src="element/images/tulip.jpg" alt="Icon"></div>
            <h2>Just because you work hard doesn't<br> mean you'll be successful.</h2>
        </div>    
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="video-holder-box-style5">
                    <div class="icon">
                        <div class="inner">
                            <a class="video-popup wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" title="RinBuild Video Gallery" href="https://www.youtube.com/watch?v=PZO94HpYa7k">
                                <span class="flaticon-play-button"></span>
                            </a>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section> 
<!--End Video Gallery Style2 Area-->

<!--Start Service Pak Ahmed-->
<!--section class="service-style1-area style4">
    <div class="container">
        <div class="title">
            <h1>Pelayanan Yang Diberikan <br> Tulip Rangkas Residence.<br> <a href="#">#WujudkanRumahmu.</a></h1>
        </div>
        <div class="row">
            <div class="col-xl-5 col-lg-12">
                <div class="row">
                   
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="single-service-style1">
                            <div class="icon-holder lazy-image">
                                <img src="element/images/service-icon-3.png" alt="Icon">
                            </div> 
                            <div class="text-holder">
                                <h3><a href="#">
Pakar Interior<br> Insinyur</a></h3>
                            </div>   
                        </div>    
                    </div>
                   
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="single-service-style1">
                            <div class="icon-holder lazy-image">
                                <img src="element/images/service-icon-1.png" alt="Icon">
                            </div> 
                            <div class="text-holder">
                                <h3><a href="#">Kwalitas<br> Bahan Untuk Rumah Bagus</a></h3>
                            </div>   
                        </div> 
                    </div>
                    
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="single-service-style1">
                            <div class="icon-holder">
                                <img src="element/images/service-icon-2.png" alt="Icon">
                            </div> 
                            <div class="text-holder">
                                <h3><a href="#">
Konstruksi &
Teknik &amp;<br> Yang Handal</a></h3>
                            </div>   
                        </div>
                    </div>
                   
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="single-service-style1">
                            <div class="icon-holder lazy-image">
                                <img src="element/images/service-icon-3.png" alt="Icon">
                            </div> 
                            <div class="text-holder">
                                <h3><a href="#">Pelayanan<br> Team Marketing Yang Ramah.</a></h3>
                            </div>   
                        </div>    
                    </div>
                  
                </div>
            </div>
            
            <div class="col-xl-7 col-lg-12">
                <div class="service-style3-image-box" data-aos="fade-left" data-aos-duration="0" data-aos-delay="0">
                    <img src="element/images/pak_ahmed.jpg" alt="Awesome Image"> 
                </div>    
            </div>

        </div>
    </div>
</section-->
<!--End Service Style1 Area-->  



<!--Start About Style1 Area-->
<section class="about-style1-area style3">
    <div class="about-style3-image-box" data-aos="fade-right" data-aos-duration="0" data-aos-delay="0">
        
    </div>  
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
            <img src="element/images/pak_ahmed.jpg" alt="Awesome Image" style="width: 600px;">  
            </div>
            <div class="col-xl-7 col-lg-12">
                <div class="about-style1-text-box style3">
                <div class="title">
                        <p></p>
                        <h1>Ahmed Yunes Abobakr Noor</h1>
                        <p style="font-size: 23PX;line-height: 20px;font-weight: 400;text-transform: uppercase;letter-spacing: 6px; margin:23px 0;">Direktur Utama</p>
                    </div>
                    <div class="inner-contant">
                        <p>Dibangun di lokasi nyaman, aman serta pastinya bebas dari Banjir. Kami mendesain khusus menggunakan bahan bangunan pilihan dan material terbaik dikelasnya.
                            <br>Beragam pilihan hunian yang tersedia dengan beberapa Type yang ditawarkan dan disesuaikan dengan kebutuhan Anda. Perumahan Komersil dengan type 54/120 dan type 39/60 serta Perumahan ber-Subsidi dengan type 27/60.
                               
                            </p>
                       <p>Percayakan kebutuhan Anda akan perumahan bagi keluarga tercinta hanya di Tulip Rangkas Residence, a comfort place to live.</p>
                   
                    </div>    
                </div>  
            </div>
            
        </div> 
    </div>    
</section>
<!--End About Style1 Area-->





 
<!--Start Team Area-->
<!--section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
            
            <div class="big-title black-clr"><h1>Team Marketing</h1></div>
        </div>
        <div class="row">
           
           
             <Start Single Team Member>
             <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/willy(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Willy </h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Supervisor</span>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=62818671900&amp;text=Assalamualaikum%2C%20Saya%20ingin%20menanyakan%20Harga%20Perumahan%20Tulip%20Rangkas%20Residence.%20Tolong%20segera%20hubungi%20saya-%20By%20Website%20TulipRangkas.com" target="_blank"><span class=" thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <End Single Team Member>
             
            
           
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/hasan.jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Hasanudin K</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                           <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700050777&amp;text=Assalamualaikum%2C%20Saya%20ingin%20menanyakan%20Harga%20Perumahan%20Tulip%20Rangkas%20Residence.%20Tolong%20segera%20hubungi%20saya-%20By%20Website%20TulipRangkas.com" target="_blank"><span class="thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <End Single Team Member> <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms;">
                    <div class="img-holder">
                        <img src="element/images/berly(team-tulip-rangkas).jpg" alt="Awesome Image">
                    </div>
                    <div class="title-holder">
                        <div class="inner">
                            <div class="left">
                                <h3>Berly</h3>
                                <div class="social-links">
                                    <ul class="social-links-style1">
                                        <li>
                                            <span class="">Sales Marketing</span>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </div>
                            <div class="right">
                                <a href="https://api.whatsapp.com/send?phone=6287700050777&amp;text=Assalamualaikum%2C%20Saya%20ingin%20menanyakan%20Harga%20Perumahan%20Tulip%20Rangkas%20Residence.%20Tolong%20segera%20hubungi%20saya-%20By%20Website%20TulipRangkas.com" target="_blank"><span class="thm_why"><img src="element/images/icon/wa.png" style="width: 40px; padding-bottom: 8px;"></span></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <End Single Team Member>
             
        </div>
    </div>
</section>
<End Team Area-->

<!--Start Penawaran Promo-->
<!--section class="slogan-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="slogan-content wow slideInUp" data-wow-delay="100ms">
                    <div class="title">
                        <h1 style="font-size: 43px;"> Penawaran Promo dengan Harga Menarik? </h1>
                    </div>
                    <div class="quote-button">
                        <a href="#">Kirimi Saya Promo <span class="flaticon-next"></span></a>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</section>
<End Penawaran Promo-->

<!--Start Testimonial style2 Area>
<section class="testimonial-style2-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>Testimonial Klien</h1></div>
        </div>
        <div class="row"> 
            <div class="col-xl-12">
                <div class="rinbuild-carousel testimonial-carousel owl-carousel owl-theme owl-dot-style1" data-options='{"loop":true, "margin":30, "autoheight":true, "nav":false, "dots":true, "autoplay":true, "autoplayTimeout":6000, "smartSpeed":500, "responsive":{ "0":{"items": "1"}, "768":{"items": "1"}, "1000":{"items": "2" }}}'>
                    <Start Single Testimonial Style2>
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Terimakasi Pelanyanan Marketing Ramah dan Sabar</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Saepul</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <End Single Testimonial Style2> 
                    <Start Single Testimonial Style2>
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Perumahan Untuk Jangka panjang yg bagus, terimakasi Tulip Rangkas Residence.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Kiki Rizkiani</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <End Single Testimonial Style2>
                    <Start Single Testimonial Style2>
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Wah Dp Gak terlalu mahal, Terimakasi.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Nina Herawati</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <End Single Testimonial Style2> 
                    <Start Single Testimonial Style2>
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Terimakasi Tulip Rangkas Akhirnya punya rumah</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Yanto Darmawan</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <End Single Testimonial Style2>
                    <Start Single Testimonial Style2>
                    <div class="single-testimonial-style1">
                        <div class="text">
                            <p>Mantap Dah Tulip, Akhirnya aku punya rumah sendiri.</p>
                        </div> 
                        <div class="client-info">
                            <div class="icon-box">
                                <span class="flaticon-engineer-1"></span>
                            </div>
                            <div class="title-box">
                                <h3>Wahyu Soenharto</h3>
                                <p>construction worker</p>
                            </div>
                        </div>   
                    </div>
                    <End Single Testimonial Style2>              
                </div>
            </div> 
              
        </div>
    </div>
</section>
<End Testimonial Style2 Area-->                             

<!--Start Partner Area-->
 
<!--End Partner Area-->  
<?php 
include 'footer.php';
?>