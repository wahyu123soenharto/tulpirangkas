<?php 
include 'head.php';
?>

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(element/images/kontak1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Career</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Career</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End breadcrumb area-->


<section class="service-imagegalley-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                
                <div class="single-service">
                    <img src="element/images/career.png" alt="Awesome Image">
                </div>
                
                
            </div>    
        </div>
    </div>    
</section>
<br><br>

<?php 
include 'footer.php';
?>