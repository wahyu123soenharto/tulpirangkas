
<!--Start Partner Area-->
<section class="partner-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>MITRA BANK</h1></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="partner-box">
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/btn.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/btn.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/bri syariah.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/bri syariah.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box marleft-minus">
                        <a href="#"><img src="element/images/mitra/Logo_BNI_Syariah.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/Logo_BNI_Syariah.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                    <!--Start Single Partner Logo Box-->
                    <div class="single-partner-logo-box">
                        <a href="#"><img src="element/images/mitra/mandiri.png" alt="Awesome Image"></a>
                        <div class="overlay-box">
                            <a href="#"><img src="element/images/mitra/mandiri.png" alt="Awesome Image"></a>    
                        </div>
                    </div>
                    <!--End Single Partner Logo Box-->
                   
                </div>
            </div>
        </div>
    </div>
</section> 
<!--End Partner Area-->  

<!--Start footer area-->  
<footer class="footer-area">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!--Start single footer widget-->
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.3s">
                    <div class="single-footer-widget">
                        <div class="our-company-info">
                            <div class="footer-logo">
                                <a href="index.html"><img src="element/images/logo/logo_putih.png" alt="Awesome Footer Logo" title="Logo"></a>    
                            </div>
                            <div class="text">
                                <p>Tulip Rangkas Residence hadir memberikan hunian yang sangat dekat dengan beberapa akses publik seperti Stasiun Rangkasbitung, Terminal, SPBU, hingga akses pendidikan.</p>
                            </div>    
                            <div class="footer-social-links">
                                <ul class="social-links-style1">
                                    <li>
                                        <a href="https://www.instagram.com/tuliprangkas.residence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                                    </li>
                                    
                                    <li>
                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a> 
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End single footer widget-->
                <!--Start single footer widget-->
                <div class="col-xl-4 col-lg-4 col-md-9 col-sm-12 wow animated fadeInUp" data-wow-delay="0.5s">
                    <div class="single-footer-widget margin50-0">
                        <div class="title">
                            <h3>Menu</h3>
                        </div>
                        <div class="pages-box">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                    <ul class="page-links">
                                        <li><a href="about-us.php">Tentang Kami</a></li>
                                        <li><a href="produk.php">Info</a></li>
                                        <li><a href="promo.php">Promo</a></li>
                                        <li><a href="tipe_rumah_54.ph">Galery</a></li>
                                        <li><a href="contact.php">Hubungi Kami</a></li>	
                                    </ul>      
                                </div>
                                <!-- <div class="col-xl-6 col-lg-6 col-md-6">
                                    <ul class="page-links">
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Departments</a></li>
                                        <li><a href="#">Timetable</a></li>
                                        <li><a href="#">Why Us</a></li>
                                        <li><a href="#">Specilaties</a></li>	
                                        <li><a href="#">Retail</a></li>	
                                    </ul>      
                                </div> -->
                            </div>    
                        </div>
                          
                    </div>
                </div>
                <!--End single footer widget-->  
                <!--Start single footer widget-->
                <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 wow animated fadeInUp" data-wow-delay="0.7s">
                    <div class="single-footer-widget">
                        <div class="twitter-feed-box">
                            <h3><a href="#">Update Unit Terjual</a></h3>
                            <span>14 Februari 2020</span>
                            <div class="border-box"></div> 
                            <img src="element/images/1.jpg" alt="Awesome Image" style="height: 200px;">
                            <div class="text">
                             </div>
                            <div class="bottom">
                                <div class="comments">
                                    <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i>2 comments</a>
                                </div>
                                <!-- <div class="twitter-icon"> -->
                                    <!-- <span class="flaticon-twitter-logo-shape"></span> -->
                                <!-- </div> -->
                            </div>
                        </div>                   	  
                    </div>
                </div>
                <!--End single footer widget-->
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="outer-box">
                <div class="copyright-text">
                    <p>Copyright© <a href="#">PT Javasiss</a></p>
                </div>
                    
            </div>    
        </div>    
    </div>
</footer>   
<!--End footer area-->

</div> 


<button class="scroll-top scroll-to-target" data-target="html">
    <span class="icon-angle"></span>
    <img src="element/images/icon/panah.png">
</button>

<script src="element/js/jquery.js"></script>
<script src="element/js/aos.js"></script>
<script src="element/js/appear.js"></script>
<script src="element/js/bootstrap.bundle.min.js"></script>
<script src="element/js/bootstrap-select.min.js"></script>
<script src="element/js/isotope.js"></script>
<script src="element/js/jquery.bootstrap-touchspin.js"></script>
<script src="element/js/jquery.countdown.min.js"></script>
<script src="element/js/jquery.countTo.js"></script>
<script src="element/js/jquery.easing.min.js"></script>
<script src="element/js/jquery.enllax.min.js"></script>
<script src="element/js/jquery.fancybox.js"></script>
<script src="element/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="element/js/jquery.mixitup.min.js"></script>
<script src="element/js/jquery.paroller.min.js"></script>
<script src="element/js/jquery.polyglot.language.switcher.js"></script>
<script src="element/js/map-script.js"></script>
<script src="element/js/nouislider.js"></script>
<script src="element/js/owl.js"></script>
<!-- <script src="element/js/timePicker.js"></script>    -->
<script src="element/js/validation.js"></script>
<script src="element/js/wow.js"></script>
<script src="element/js/jquery.magnific-popup.min.js"></script>
<script src="element/js/slick.js"></script>
<script src="element/js/lazyload.js"></script>
<!-- thm custom script -->
<script src="element/js/custom.js"></script>
</body>
</html>