<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/ruko/ruko.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Ruko </h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Ruko</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="latest-portfolio-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>Produk Tulip Rangkas Residence</h1></div>
        </div>
        <div class="row">
        <div class="col-xl-12">
                <div class="project-menu-box wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <ul class="project-filter clearfix post-filter has-dynamic-filters-counter">
                    <li><i style="position: relative;top: 3px; display: inline-block; font-size: 30px; padding-right: 6px;" class="flaticon-modern-bridge-road-symbol"></i><a href="ruko.php">Ruko</a></li>
                    <li><i style="position: relative;top: 3px; display: inline-block; font-size: 30px; padding-right: 6px;" class="flaticon-house"></i><a href="pro_54.php">Tipe 54</a></li>
                    <li><i style="position: relative;top: 3px; display: inline-block; font-size: 30px; padding-right: 6px;" class="flaticon-house"></i><a href="pro_39.php">Tipe 39</a></li>
                    <li><i style="position: relative;top: 3px; display: inline-block; font-size: 30px; padding-right: 6px;" class="flaticon-house"></i><a href="pro_27.php">Tipe 27</a></li>
                   
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="col-xl-12 col-lg-12 col-md-12  54">
                <!--Start breadcrumb area-->     
                <section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
            <section class="video-gallery-style2-area1">
    <div class="video-galler-outer-bg" style="background-image:url(element/images/GATE 1 EDIT .png);">
        <div class="title-holder text-center">
            <div class="icon"><img src="element/images/ruko1.jpg" alt="Icon"></div>
        </div>    
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="video-holder-box-style51">
                    <div class="icon">
                        <div class="inner">
                            <a class="video-popup wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" title="RinBuild Video Gallery" href="https://www.youtube.com/watch?v=3ujpTjSfDZ8">
                                <span class="flaticon-play-button"></span>
                            </a>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>    
</section>   
            </div> 
            <div class="col-xl-4">
                <div class="team-member-info-box12">
                <div class="title">
                        <h3>Detail Bangunan Ruko</h3>
                                           </div>
                    <ul>
                       <li><span style="color: #610053;">SPESIFIKASI DETAIL RUKO</span></li>
                       <li><span>Pondasi:</span>Batu Kali + Slop Beton</li>
                        <li><span>Dinding:</span> Hebel</li>
                        <li><span>Cat :</span> Propan Paint</li>
                        <li><span>Keramik :</span> 40x40 Asia Tile</li>
                        <li><span>Keramik KM :</span> 40x40 + 25x40 platinum tile</li>
                        <li><span>Atap:</span>  Baja Ringan + Spandek </li>
                        <li><span>Plafon:</span> Hollow + Gypsum Board</li>
                        <li><span>Pintu :</span> Aluminium + Kaca</li>
                        <li><span>Kloset:</span> : Duduk  American standard </li>
                        <li><span>Jendela:</span> Aluminium + Kaca</li>
                        <li><span>Pintu KM:</span> TITAN  PVC DOOR</li>
                        <li><span>Listrik:</span> 1300KWH</li>
                        <li><span>Air:</span>Tanah</li>
                    </ul>
                    <!-- <ul>
                        <li><span>Work progress:</span> 100%</li>
                        <li><span>Email:</span> <a href="mailto:info@templatepath.com">alex.lipson@gmail.com</a></li>
                    </ul>
                    <ul>
                        <li><span>Phone:</span> +555 666 77 88 99</li>
                    </ul>-->
                    <ul class="social-links-style1">
                        <li>
                            <a href="https://www.facebook.com/tulip.rangkasresidence.5" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="https://www.instagram.com/tuliprangkas.residence/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                        </li>
                    </ul> 
                </div>    
            </div>       
        </div>
        

       </section>
       <br><br>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUKO</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/ruko/depan.jpg" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/ruko/2.jpg" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
            </div>
            
<?php 
include 'footer.php';
?>