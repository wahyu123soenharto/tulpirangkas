
<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/projek-berjalan/anthony-fomin-sTw2KYpoujk-unsplash.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Produk </h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Produk</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="latest-portfolio-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>Produk Tulip Rangkas Residence</h1></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="project-menu-box wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <ul class="project-filter clearfix post-filter has-dynamic-filters-counter">
                        <li data-filter=".54"><span class="filter-text"><i class="flaticon-house"></i>Tiper rumah 54</span></li>
                        <li data-filter=".39"><span class="filter-text"><i class="flaticon-house"></i>Tiper rumah 39</span></li>
                        <li data-filter=".27"><span class="filter-text"><i class="flaticon-house"></i>Tiper rumah 27</span></li>
               
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row filter-layout masonary-layout">
             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-12 col-lg-12 col-md-12  27">
                <!--Start breadcrumb area-->     
                <section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="team-singel-image-box">
                    <img src="element/images/27/tipe_27.jpg" alt="Awesome Image">    
                </div>    
            </div> 
            <div class="col-xl-4">
                <div class="team-member-info-box">
                    <div class="title">
                        <h3>Rumah Tipe 27/60</h3>
                                           </div>
                    <ul>
                                       <li><span style="color: #610053;">SPESIFIKASI RUMAH</span></li>
                       <li><span>2 Kamar Tidur</span></li>
                        <li><span>1 Kamar Mandi</span> </li>
                        <li><span>1 Parkir Mobil</span></li>
                       
<br>
                        <li><span style="color: #610053;">SPESIFIKASI RUMAH DETAIL</span></li>
                        <li><span>Pondasi:</span>Batu kali</li>
                        <li><span>Dinding:</span> Bata Ringan (Hebel)</li>
                        <li><span>Cat Dlm & Luar:</span>Vinilex Paint</li>
                        <li><span>Lantai</span> : Keramik 30x30</li>
                        <li><span>Atap:</span>  Baja Ringan + Genteng Mantili</li>
                        <li><span>Plafon:</span> Gypsum</li>
                        <li><span>Kloset KM Utama:</span> Jongkok</li>
                        <li><span>Kusen Pintu:</span> Aluminium</li>
                        <li><span>Jendela:</span> Aluminium</li>
                        <li><span>Pintu Kamar Mandi:</span> PVC</li>
                        <li><span>Listrik:</span> 900KWH</li>
                        <li><span>Air:</span>Tanah</li>
                    </ul>
                    <!-- <ul>
                        <li><span>Work progress:</span> 100%</li>
                        <li><span>Email:</span> <a href="mailto:info@templatepath.com">alex.lipson@gmail.com</a></li>
                    </ul>
                    <ul>
                        <li><span>Phone:</span> +555 666 77 88 99</li>
                    </ul>-->
                    <ul class="social-links-style1">
                        <li>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                        </li>
                    </ul> 
                </div>    
            </div>       
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="team-member-content-box">
                    <h3>Rumah Tipe 27/60</h3>
                    <p>
                    Beragam tipe perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Komersil dengan Tipe 27/60, harga yang ditawarkan mulai dari IDR 150,5 jt. Lokasi strategis di kawasan Lebak, Banten. Hubungi team Marketing kami untuk mengetahui info lebih detail dengan promo harga terbaik.
   
                </p>
                    
                </div>
            </div>
        </div>

       </section>
       <section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>INTERIOR RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/ruang tamu.jpg" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.4s" data-wow-duration="1400ms" style="visibility: visible; animation-duration: 1400ms; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/kamar mandi.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/dapur.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
          
        </div>
    </div>
</section>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/27/denah_27.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
            </div>
             <!--End Single tipe 27-->
             
              
   <!--Start Single Tipe Rumah 54-->
   <div class="col-xl-12 col-lg-12 col-md-12  39">
                <!--Start breadcrumb area-->     
                <section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="team-singel-image-box">
                    <img src="element/images/39/tipe_rumah_39.jpg" alt="Awesome Image">    
                </div>    
            </div> 
            <div class="col-xl-4">
                <div class="team-member-info-box">
                    <div class="title">
                        <h3>Rumah Tipe 39/60</h3>
                                           </div>
                    <ul>
                                       <li><span style="color: #610053;">SPESIFIKASI RUMAH</span></li>
                       <li><span>2 Kamar Tidur</span></li>
                        <li><span>2 Kamar Mandi</span> </li>
                        <li><span>1 Parkir Mobil</span></li>
                        <li><span>1 Dapur</span></li>
<br>
                        <li><span style="color: #610053;">SPESIFIKASI RUMAH DETAIL</span></li>
                        <li><span>Pondasi:</span>Batu kali</li>
                        <li><span>Dinding:</span> Bata Ringan (Hebel)</li>
                        <li><span>Cat Dlm & Luar:</span> Nippon Paint</li>
                        <li><span>Lantai</span> : Keramik 40x40</li>
                        <li><span>Atap:</span>  Baja Ringan</li>
                        <li><span>Plafon:</span> Gypsum</li>
                        <li><span>Kloset KM Utama:</span> Duduk</li>
                        <li><span>Kusen Pintu:</span> Aluminium</li>
                        <li><span>Jendela:</span> Aluminium</li>
                        <li><span>Pintu Kamar Mandi:</span> PVC</li>
                        <li><span>Listrik:</span> 1300KWH</li>
                        <li><span>Air:</span>Tanah</li>
                    </ul>
                    <!-- <ul>
                        <li><span>Work progress:</span> 100%</li>
                        <li><span>Email:</span> <a href="mailto:info@templatepath.com">alex.lipson@gmail.com</a></li>
                    </ul>
                    <ul>
                        <li><span>Phone:</span> +555 666 77 88 99</li>
                    </ul>-->
                    <ul class="social-links-style1">
                        <li>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                        </li>
                    </ul> 
                </div>    
            </div>       
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="team-member-content-box">
                    <h3>Rumah Tipe 39/60</h3>
                    <p>
                    Beragam tipe perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Komersil dengan Tipe 39/60, harga yang ditawarkan mulai dari IDR 225 jt. Lokasi strategis di kawasan Lebak, Banten. Hubungi team Marketing kami untuk mengetahui info lebih detail serta promo harga terbaik </p>
                    
                </div>
            </div>
        </div>

       </section>
       <section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>INTERIOR RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/ruang tamu.jpg" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.4s" data-wow-duration="1400ms" style="visibility: visible; animation-duration: 1400ms; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/kamar mandi.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/39/dapur.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
          
        </div>
    </div>
</section>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/39/FULL_DENAH_TYPE_39.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
            </div>
        
            <!--Start Single Tipe Rumah 54-->
            <div class="col-xl-12 col-lg-12 col-md-12  54">
                <!--Start breadcrumb area-->     
                <section class="team-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="team-singel-image-box">
                    <img src="element/images/54/Type 54 120.jpg" alt="Awesome Image">    
                </div>    
            </div> 
            <div class="col-xl-4">
                <div class="team-member-info-box">
                    <div class="title">
                        <h3>Rumah Tipe 54/120</h3>
                                           </div>
                    <ul>
                                       <li><span style="color: #610053;">SPESIFIKASI RUMAH</span></li>
                       <li><span>3 Kamar Tidur</span></li>
                        <li><span>2 Kamar Mandi</span> </li>
                        <li><span>1 Parkir Mobil</span></li>
                        <li><span>1 Dapur</span></li>
<br>
                        <li><span style="color: #610053;">SPESIFIKASI RUMAH DETAIL</span></li>
                        <li><span>Pondasi:</span>Batu kali</li>
                        <li><span>Dinding:</span> Bata Ringan (Hebel)</li>
                        <li><span>Cat Dlm & Luar:</span> Nippon Paint</li>
                        <li><span>Lantai</span> : Keramik</li>
                        <li><span>Atap:</span>  Baja Ringan</li>
                        <li><span>Plafon:</span> Gypsum</li>
                        <li><span>Kloset KM Utama:</span> Duduk</li>
                        <li><span>Kloset KM Tamu:</span> Jongkok</li>
                        <li><span>Kusen Pintu:</span> Aluminium</li>
                        <li><span>Jendela:</span> Aluminium</li>
                        <li><span>Pintu Kamar Mandi:</span> PVC</li>
                        <li><span>Listrik:</span> 1300KWH</li>
                        <li><span>Air:</span>Tanah</li>
                    </ul>
                    <!-- <ul>
                        <li><span>Work progress:</span> 100%</li>
                        <li><span>Email:</span> <a href="mailto:info@templatepath.com">alex.lipson@gmail.com</a></li>
                    </ul>
                    <ul>
                        <li><span>Phone:</span> +555 666 77 88 99</li>
                    </ul>-->
                    <ul class="social-links-style1">
                        <li>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> 
                        </li>
                    </ul> 
                </div>    
            </div>       
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="team-member-content-box">
                    <h3>Rumah Tipe 54/120</h3>
                    <p>
                    Beragam tipe perumahan yang ditawarkan di Tulip Rangkas Residence. Salah satunya untuk Perumahan Komersil dengan Tipe 54/120, harga yang ditawarkan mulai dari IDR 399 jt. Lokasi strategis di kawasan Lebak, Banten. Hubungi team Marketing kami untuk info lebih detail serta promo yang sedang berlangsung
                    </p>
                    
                </div>
            </div>
        </div>

       </section>
       <section class="team-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>INTERIOR RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/54/kamar tidur.jpg" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.4s" data-wow-duration="1400ms" style="visibility: visible; animation-duration: 1400ms; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/54/kamar mandi.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
            <!--Start Single Team Member-->
            <div class="col-xl-4 col-lg-4 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0.5s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/interior/54/dapur.jpg" alt="Awesome Image">
                    </div>
                   
                </div>
            </div>
            <!--End Single Team Member-->
          
        </div>
    </div>
</section>
<section class="team-area">
    <div class="container">
        <div class="sec-title1 text-center">
            <div class="big-title black-clr"><h1>DENAH RUMAH</h1></div>
        </div>
        <div class="row">
            <!--Start Single Team Member-->
            <div class="col-xl-12 col-lg-12 wow fadeInUp animated animated" data-wow-delay="0.3s" data-wow-duration="1300ms" style="visibility: visible; animation-duration: 1300ms; animation-delay: 0.3s; animation-name: fadeInUp;">
                <div class="single-team-member wow fadeInUp animated" data-wow-delay="100ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 100ms; animation-name: fadeInUp;">
                    <div class="img-holder">
                        <img src="element/images/54/REVISI-DENAH.png" alt="Awesome Image">
                    </div>
                    
                </div>
            </div>
            <!--End Single Team Member-->
                     
        </div>
    </div>
</section>
            </div>
        


<?php 
include 'footer.php';
?>