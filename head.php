
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tulip Rangkas Residence</title>

	<!-- responsive meta -->
	<meta name="wahyu" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="Tulip Rangkas Residence" content="IE=edge">
    <link rel="stylesheet" href="element/css/custom.css">
	<link rel="stylesheet" href="element/css/aos.css">
	<link rel="stylesheet" href="element/css/bootstrap.min.css">	
	<link rel="stylesheet" href="element/css/imp.css">
	<link rel="stylesheet" href="element/css/custom-animate.css">
	<link rel="stylesheet" href="element/css/flaticon.css">
	<!-- <link rel="stylesheet" href="element/fontawesome/css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="element/css/owl.css">
	<link rel="stylesheet" href="element/css/magnific-popup.css">
   
    <link rel="stylesheet" href="element/css/color.css">
    <link rel="stylesheet" href="element/css/style.css">
	<link rel="stylesheet" href="element/css/responsive.css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="element/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="element/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="element/images/favicon-16x16.png" sizes="16x16">

      
</head>

<body>
<div class="boxed_wrapper">

<div class="preloader"></div> 

<!-- main header -->
<header class="main-header"> 
    <div class="header-top">
        <div class="container">
            <div class="outer-box">
                <div class="header-top-left">
                    <div class="welcome-text">
                        <h6><marquee>Unit Terbatas! Promo DP hanya 5jt untuk rumah tipe 39 & tipe 54. Hubungi team marketing kami sekarang!</marquee> </h6> 
                    </div>       
                </div>
                <!--div class="header-top-middle">
                    <div class="header-social-links">
                        <ul class="social-links-style1">
                            <li>
                                <a href="https://www.facebook.com/tulip.rangkasresidence.5" target="_blank"><span class="flaticon-facebook-1 fb"></span></a> 
                            </li>
                           
                            <li>
                                <a href="https://www.instagram.com/tuliprangkas.residence/" target="_blank"><span class="flaticon-instagram-1"></span></a> 
                            </li>
                            
                        </ul>
                    </div>        
                </div-->
                <div class="header-top-right">
                    <div class="header-menu">
                        <ul>
                            <li><a href="career.php">Career</a></li>
                            <li>
                                <a href="https://www.facebook.com/tulip.rangkasresidence.5" target="_blank"><span class="flaticon-facebook-1 fb"></span></a> 
                            </li>
                           
                            <li>
                                <a href="https://www.instagram.com/tuliprangkas.residence/" target="_blank"><span class="flaticon-instagram-1"></span></a> 
                            </li>
                        </ul>
                    </div>            
                </div>         
            </div>    
        </div>
    </div>
    
    <!--Start Header upper -->
    <div class="header-upper">
        <div class="container clearfix">
            <div class="outer-box clearfix"> 
                <div class="header-upper-left float-left clearfix">
                    <div class="logo">
                        <a href="index.html"><img src="element/images/logo/logo.png" alt="Awesome Logo" title="" style="width: 260px;"></a>
                    </div>
                </div>
                <div class="header-upper-right float-right clearfix">
                    <div class="header-contact-info">
                        <ul>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-call thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Call or Chat (WhatsApp)</span>
                                    <h6><a href="tel:123456789">0818 671 900</a></h6>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-clock thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Marketing Office Hour</span>
                                    <h6>Mon – Sat, from 09:00 to 18:00 </h6>
                                </div>
                            </li>
                            <li>
                                <div class="icon">
                                    <span class="flaticon-mail thm_clr1"></span>
                                </div>
                                <div class="title">
                                    <span class="thm_clr1">Email us now</span>
                                    <h6><a href="mailto:info@tuliprangkas.com ">info@tuliprangkas.com </a></h6>
                                </div>
                            </li>
                        </ul>    
                    </div> 
                       
                </div>
            </div>
        </div>
    </div>
    <!--End Header Upper-->
    
    <!--Start header lawer -->
    <div class="header-lawer">
        <div class="container clearfix">
            <div class="outer-box clearfix">
                <!--Top Left-->
                <div class="header-lawer-left float-left">
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li>
                                        <a class="home-icon" href="index.php">
                                            <span class="hometext">Home</span>
                                            <span class="flaticon-real-estate homeicon"></span>
                                        </a>
                                    </li>
                                    <li><a href="about-us.php">Tentang Kami</a>
                                       
                                    </li>
                                    <li class="dropdown"><a href="#">info</a>
                                        <ul>
                                        <li><a href="produk.php">Produk</a></li>
                                        <li><a href="rumah-terjual.php">Unit Terjual</a></li>
                                        <li><a href="progres-pembangunan.php">Progres Pembangunan</a></li>
                                        <li><a href="fasilitas.php">Fasilitas</a></li>  
                                        </ul>
                                    </li>
                                    <li><a href="promo.php">Promo</a>
                                    <li class="dropdown"><a href="#">Galery</a>
                                        <ul>
                                        <li><a href="ruko_pro.php">Ruko</a></li>
                                            <li><a href="tipe_rumah_54.php">Tipe 54/120</a></li>
                                            <li><a href="tipe_39.php">Tipe 39/60</a></li>
                                            <li><a href="tipe_27.php">Tipe 27/60</a></li>
                                        </ul>
                                    </li>
                                    <!--li><a href="site_plant.php">Site Plane</a>
                                    	<ul>
                                            <li class="dropdown"><a href="#">Portfolio</a>
                                                <ul>
                                                    <li><a href="portfolio-v1.html">Portfolio Style1 Page</a></li>
                                                    <li><a href="portfolio-v2.html">Portfolio Style2 Page</a></li>
                                                    <li><a href="portfolio-single-v1.html">Portfolio Details Style1</a></li>
                                                    <li><a href="portfolio-single-v2.html">Portfolio Details Style2</a></li>
                                                </ul>    
                                            </li>
                                           
                                        </ul>
                                    </li-->
                                   
                                    	
                                    </li>
                                    <li><a href="contact.php">Hubungi Kami</a></li>
                                </ul>
                            </div>
                        </nav>                        
                        <!-- Main Menu End-->
                    </div>      
                </div>
                <!--Top Right-->
                <div class="header-lawer-right clearfix float-right">
                    <div class="outer-search-box-style1">
                        <div class="seach-toggle"><i class="fa fa-search"></i></div>
                        <ul class="search-box">
                            <li>
                                <form method="post" action="index.php">
                                    <div class="form-group">
                                        <input type="search" name="search" placeholder="Search Here" required="">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>    
                    <div class="quote-button">
                        <a href="#">Cari<span class="flaticon-next"></span></a>
                    </div>    
                </div>
            </div>  
        </div>
    </div>
    <!--End header lawer -->
   
    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="container">
            <div class="clearfix">
                <!--Logo-->
                <div class="logo float-left">
                    <a href="index.php" class="img-responsive"><img src="element/images/logo/logo.png" alt="" title=""></a>
                </div>
                <!--Right Col-->
                <div class="right-col float-right">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li >
                                    <a class="home-icon" href="index.php"><span class="hometext">Home</span><span class="flaticon-real-estate homeicon"></span></a>
                                    
                                </li>
                                <li><a href="about-us.php">Tentang Kami</a>
                                
                                    
                                </li>
                                <li class="dropdown"><a href="#">Info</a>
                                    <ul>
                                    <li><a href="produk.php">Produk</a></li>
                                        <li><a href="rumah-terjual.php">Unit Terjual</a></li>
                                        <li><a href="progres-pembangunan.php">Progres Pembangunan</a></li>
                                        <li><a href="fasilitas.php">Fasilitas</a></li>  
                                    </ul>
                                </li>
                                <li><a href="promo.php">Promo</a>
                                <li class="dropdown"><a href="#">Galery</a>
                                    <ul>
                                        <!--li class="dropdown"><a href="#">Portfolio</a-->
                                            <!-- <ul> -->
                                            <li><a href="ruko_pro.php">Ruko</a></li>
                                                <li><a href="tipe_rumah_54.php">Tipe 54/120</a></li>
                                                <li><a href="tipe_39.php">Tipe 39/60</a></li>
                                                <li><a href="tipe_27.php">Tipe 27/60</a></li>
                                            <!-- </ul>     -->
                                        </li>
                                    </ul>
                                </li>
                                <!--li ><a href="site_plant.php">Site Plane</a>
                                    < <ul>
                                        <li><a href="blog.html">Blog Grid View</a></li>
                                        <li><a href="blog-v2.html">Blog Arcive View</a></li>
                                        <li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li-->
                                <li><a href="contact.php">Hubungi Kami</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
    </div>
    <!--End Sticky Header-->
</header>
