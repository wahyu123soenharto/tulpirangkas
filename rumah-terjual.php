<?php 
include 'head.php';
?>
<section class="breadcrumb-area" style="background-image: url(element/images/unit-terjual.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title">
                       <h1>Unit Terjual</h1>
                    </div>
                    <div class="breadcrumb-menu">
                        <ul class="clearfix">
                            <li><a href="index.php">Home Back</a></li>
                            <li><span class="flaticon-next-1"></span></li>
                            <li class="active">Rumah Terjual</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="latest-portfolio-area">
    <div class="container">
        <div class="sec-title text-center">
            <div class="big-title black-clr"><h1>UNIT TERJUAL</h1></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="project-menu-box wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                    <ul class="project-filter clearfix post-filter has-dynamic-filters-counter">
                        <li data-filter=".filter-item" class="active"><span class="filter-text"><i class="flaticon-menu"></i>All</span></li>
                        <li data-filter=".ruko"><span class="filter-text"><i class="flaticon-building"></i>Ruko</span></li>
                        <li data-filter=".kafling"><span class="filter-text"><i class="flaticon-modern-bridge-road-symbol"></i>Kavling</span></li>
                        <li data-filter=".54"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 54</span></li>
                        <li data-filter=".39"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 39</span></li>
                        <li data-filter=".27"><span class="filter-text"><i class="flaticon-house"></i>Tipe rumah 27</span></li>
               
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row filter-layout masonary-layout">
              <!--Start  kafling-->
              <div class="col-xl-4 col-lg-6 col-md-6 filter-item kafling ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/1.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End  kafling-->
             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/1.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->
             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/2.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->

              <!--Start Single Tipe Rumah 27-->
              <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/3.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->

             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/4.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->
             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 27 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/5.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 27</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->
            
             <!--Start Single Tipe Rumah 39-->
<div class="col-xl-4 col-lg-6 col-md-6 filter-item kafling ">
    <div class="single-portfolio-style1">
        <div class="img-holder">
            <div class="inner-box">
                <img src="element/images/39/3.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                    <div class="box">
                        <div class="inner"> 
                            <div class="zoom-button">
                                <a class="lightbox-image" data-fancybox="gallery" href="#">
                                    <span class="flaticon-plus"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title-holder">
                <span class="tag">Kafling</span>
                <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                    <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
            </div>
        </div>
    </div>
</div>
<!--End Single tipe 39--> 
            
             <!--Start Single Tipe Rumah 27-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item kafling ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/27/5.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Kafling</span>
                            <h5><a href="#">Booking Unit untuk tipe 27/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 27-->

             <!--Start Single Tipe Rumah 39-->
            <div class="col-xl-4 col-lg-6 col-md-6 filter-item 39 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/39/1.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 39</span>
                            <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!--End Single tipe 39-->
             <!--Start Single Tipe Rumah 39-->
             <div class="col-xl-4 col-lg-6 col-md-6 filter-item 39 ">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/39/2.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Rumah Tipe 39</span>
                            <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single tipe 39-->                   
<!--Start Single Tipe Rumah 39-->
<div class="col-xl-4 col-lg-6 col-md-6 filter-item 39 ">
    <div class="single-portfolio-style1">
        <div class="img-holder">
            <div class="inner-box">
                <img src="element/images/39/3.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                    <div class="box">
                        <div class="inner"> 
                            <div class="zoom-button">
                                <a class="lightbox-image" data-fancybox="gallery" href="#">
                                    <span class="flaticon-plus"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title-holder">
                <span class="tag">Rumah Tipe 39</span>
                <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                    <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
            </div>
        </div>
    </div>
</div>
<!--End Single tipe 39-->  
<!--Start Single Tipe Rumah 39-->
<div class="col-xl-4 col-lg-6 col-md-6 filter-item 39 ">
    <div class="single-portfolio-style1">
        <div class="img-holder">
            <div class="inner-box">
                <img src="element/images/39/4.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                    <div class="box">
                        <div class="inner"> 
                            <div class="zoom-button">
                                <a class="lightbox-image" data-fancybox="gallery" href="#">
                                    <span class="flaticon-plus"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title-holder">
                <span class="tag">Rumah Tipe 39</span>
                <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                    <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
            </div>
        </div>
    </div>
</div>
<!--End Single tipe 39-->  
 
<!--Start Single Tipe Rumah 39-->
<div class="col-xl-4 col-lg-6 col-md-6 filter-item ruko ">
    <div class="single-portfolio-style1">
        <div class="img-holder">
            <div class="inner-box">
                <img src="element/images/39/4.jpg" alt="Awesome Image">
                <div class="overlay-style-one">
                    <div class="box">
                        <div class="inner"> 
                            <div class="zoom-button">
                                <a class="lightbox-image" data-fancybox="gallery" href="#">
                                    <span class="flaticon-plus"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title-holder">
                <span class="tag">Ruko</span>
                <h5><a href="#">Booking Unit untuk tipe 39/60</a></h5>
                    <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
            </div>
        </div>
    </div>
</div>
<!--End Single tipe 39--> 
         <!--Start Single Tipe Rumah 54-->
            <div class="col-xl-4 col-lg-6 col-md-6 filter-item 54">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/54_2.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                            <div class="title-holder">
                                <span class="tag">Rumah Tipe 54</span>
                                <h5><a href="#">Tipe 54 / 120</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                            </div>
                       
                    </div>
                </div>
            </div>
           <!--End Single tipe 54-->            
           
           <!--Start Single Tipe Rumah 54-->
            <div class="col-xl-4 col-lg-6 col-md-6 filter-item 54">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/54_3.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            
                                <span class="tag">Rumah Tipe 54</span>
                                <h5><a href="#">Booking Unit untuk tipe 54/120</a></h5>
                                <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single portfolio Style1-->
            <!--Start Single Tipe Rumah 54-->
            <div class="col-xl-4 col-lg-6 col-md-6 filter-item 54">
                <div class="single-portfolio-style1">
                    <div class="img-holder">
                        <div class="inner-box">
                            <img src="element/images/1.jpg" alt="Awesome Image">
                            <div class="overlay-style-one">
                                <div class="box">
                                    <div class="inner"> 
                                        <div class="zoom-button">
                                            <a class="lightbox-image" data-fancybox="gallery" href="#">
                                                <span class="flaticon-plus"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="title-holder">
                            <span class="tag">Perumahan</span>
                            <h5><a href="#">Tipe 54 / 120</a></h5>
                            <p><span class="flaticon-location-pin"></span>Jl.Maulana Hasanudin, Desa Kaduagung, Lebak Banten.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Single portfolio Style1-->               
        </div>
        
        <div class="single-blog-post-style3 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1200ms">
                        <div class="img">
                            <img src="element/images/SITE PLANE/site plane.jpg" alt="Awesome Image">
                            
                        </div>
                        
                    </div>
    </div>
</section>   
<!--End Latest Portfolio Area--> 
<?php 
include 'footer.php';
?>